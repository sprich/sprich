# sprich

## Overview

Prototype/proof-of-concept of a very simple, low-latency voice chat.
Current audio round trip time using two (ubuntu 20.04) machines connected in a wired LAN is 60ms. (I.e. sounds travel from the mic on machine 1 -> network -> speaker on machine 2 -> mic on machine 2 -> network -> speaker on machine 1 -> mic on machine 1)

## Compiling

### On and for Linux (ubuntu)
``` bash
sudo apt install portaudio19-dev libglib2.0-dev
make
```

### For Windows

#### Via Docker
``` bash
docker-compose build win
docker-compose run win make win-release
```

#### On Windows
1. Install Cygwin and install the following packages during Cygwin installation:
   - make
   - gcc
   - g++
   - libglib2.0-devel
2. Open a Cygwin terminal and `cd` to your local clone of this repository
3. Run `make`

### On and for macOS
``` bash
brew install pkg-config glib
make
```

**A note about running the client:**\
The app that starts the client program needs the Microphone permission. See *System preferences* -> *Security & Privacy* -> *Microphone*.
The app that starts the program has to be in that list.
Run `./build/client` in Terminal. Do not use other terminal programs like the integrated terminal in Visual Studio Code because Visual Studio Code does not ask for the Microphone permission.

## Usage
`./build/client [-h|--help] [--local-port LOCAL_PORT] [--remote-ip REMOTE_IP] [--remote-port REMOTE_PORT]`

Where:
- `LOCAL_PORT` is the port that the local UDP socket should bind to and therefore the UDP port both at which data is received and from which data is sent. Defaults to `1234`.
- `REMOTE_IP` is the IP that data should be sent to. Defaults to `127.0.0.1`.
- `REMOTE_PORT` is the port that data should be sent to. Defaults to `1234`.

## Examples

### Voice call between two machines on the same LAN
Suppose we have two machines connected by a LAN:
1. Machine `A` with IP address `10.0.0.1/24`
2. Machine `B` with IP address `10.0.0.2/24`

And we want to set up a bidirectional voice call between them. To do so, we run:
1. On machine `A`: `./build/client --local-port 1111 --remote-ip 10.0.0.2 --remote-port 2222`
2. And on machine `B`: `./build/client --local-port 2222 --remote-ip 10.0.0.1 --remote-port 1111`

Machine `A` will send the audio data it records from its local audio input (e.g. a microphone) via UDP to `10.0.0.2:2222`, machine `B` will do the same and send its data to `10.0.0.1:1111`. Machine `A` receives the data sent by `B` on its local socket listening on port `1111` and outputs it through its local audio output (e.g. headphones), `B` receives the data sent by `A` on its local socket listening on port `2222` and also outputs it through its local audio output.

## Implementation TODOs
1. Encryption
2. Compression
3. Forward error correction and/or fast resending of lost packets
4. GUI
5. (Web?) Management frontend
