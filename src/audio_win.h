#ifndef SPRICH_AUDIO_WIN_H
#define SPRICH_AUDIO_WIN_H

#ifdef __cplusplus
extern "C" {
#endif // #ifdef __cplusplus

#ifdef SPRICH_OS_WIN

#include <glib.h>

#include "ring_buffer.h"
#include "audio_format.h"

typedef struct {
    void *internal_data;
    audio_format format;
} audio_win_stream;

bool audio_win_init();
audio_win_stream *audio_win_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *record_stream_async_queue);
audio_win_stream *audio_win_playback_stream_new(ring_buffer *ring_buffer);
void audio_win_stream_free(audio_win_stream *stream);
void audio_win_stop();

#endif // #ifdef SPRICH_OS_WIN

#ifdef __cplusplus
}
#endif // #ifdef __cplusplus

#endif // #ifndef SPRICH_AUDIO_WIN_H
