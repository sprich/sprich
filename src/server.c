
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <gio/gio.h>
#include <stdbool.h>

#include "constants.h"
#include "ring_buffer.h"
#include "socket.h"
#include "pcm_code.h"

typedef struct {
    uint64_t id;
    uint64_t address_key;
    GSocketAddress *socket_address;
    ring_buffer *receive_ring_buffer;
} peer_info;

static uint64_t next_peer_id = 0;

uint64_t inet_socket_address_to_uint64(GInetSocketAddress *address) {

    uint64_t n = 0;

    n |= g_inet_socket_address_get_port(address);

    GInetAddress *inet_address = g_inet_socket_address_get_address(address);
    const guint8 *ip_address = g_inet_address_to_bytes(inet_address);
    gsize ip_address_size = g_inet_address_get_native_size(inet_address);

    for(unsigned int i = 0; i < ip_address_size; i++) {
        n <<= 8;
        n |= *ip_address;
        ip_address++;
    }

    return n;

}

peer_info *peer_info_new(uint64_t address_key, GInetSocketAddress *inet_socket_address) {

    ring_buffer *receive_ring_buffer = ring_buffer_new();
    if(receive_ring_buffer == NULL) {
        return NULL;
    }

    peer_info *peer = malloc(sizeof(peer_info));
    if(peer == NULL) {
        return NULL;
    }

    guint16 port = g_inet_socket_address_get_port(inet_socket_address);
    GInetAddress *ip_address = g_inet_socket_address_get_address(inet_socket_address);
    GSocketFamily ip_address_family = g_inet_address_get_family(ip_address);
    const guint8 *ip_address_bytes = g_inet_address_to_bytes(ip_address);
    gsize ip_address_bytes_size = g_inet_address_get_native_size(ip_address);

    guint8 *copied_ip_address_bytes = malloc(ip_address_bytes_size);
    if(copied_ip_address_bytes == NULL) {
        return NULL;
    }

    memcpy(copied_ip_address_bytes, ip_address_bytes, ip_address_bytes_size);

    GInetAddress *copied_ip_address = g_inet_address_new_from_bytes(copied_ip_address_bytes, ip_address_family);

    peer->id = next_peer_id++;
    peer->address_key = address_key;
    peer->socket_address = g_inet_socket_address_new(copied_ip_address, port);
    peer->receive_ring_buffer = receive_ring_buffer;

    return peer;

}

static gint local_port = 1234;

static GOptionEntry entries[] = {
    {"local-port", 0, 0, G_OPTION_ARG_INT, &local_port, "Local port to bind to", NULL},
    {NULL}
};

int main(int argc, char **argv) {
    (void) argc;

    bool success;

    GError *error;

    GOptionContext *context;
    gchar **args;

#ifdef G_OS_WIN32
    args = g_win32_get_command_line ();
    (void) argv;
#else
    args = g_strdupv(argv);
#endif

    context = g_option_context_new(NULL);
    g_option_context_add_main_entries(context, entries, NULL);

    if(!g_option_context_parse_strv(context, &args, &error)) {
        fprintf(stderr, "%s\n", error->message);
        return EXIT_FAILURE;
    }

    g_strfreev(args);

    printf("local_port=%d\n", local_port);

    socket socket;

    success = socket_init(&socket, local_port, false);
    if(!success) {
        return EXIT_FAILURE;
    }

    //

    GHashTable *peers_hash_table = g_hash_table_new(g_int64_hash, g_int64_equal); // use (pointers to) int64 values as keys
    if(peers_hash_table == NULL) {
        fprintf(stderr, "could not g_hash_table_new\n");
        return EXIT_FAILURE;
    }

    // TODO periodically remove inactive peers

    //

    GTimer *timer = g_timer_new();
    if(timer == NULL) {
        fprintf(stderr, "could not g_timer_new\n");
        return EXIT_FAILURE;
    }

    uint64_t total_num_samples_sent = 0;

    for(;;) {

        bool idle = true;

        // first, handle incoming data
        for(;;) {
            uint8_t buffer[0x10000];
            GSocketAddress *address;
            gssize read = g_socket_receive_from(socket.gsocket, &address, (gchar *) buffer, sizeof(buffer), NULL, NULL);

            if(read < 0) {
                // socket is non blocking
                // if read is negative, it probably means there is no data available
                break;
            }

            idle = false;

            //printf("received %ld bytes\n", read);

            if(g_socket_address_get_family(address) != G_SOCKET_FAMILY_IPV4) {
                continue;
            }

            uint64_t address_key = inet_socket_address_to_uint64((GInetSocketAddress *) address);
            peer_info *peer = g_hash_table_lookup(peers_hash_table, &address_key);

            if(peer == NULL) {
                printf("new peer\n");

                peer = peer_info_new(address_key, (GInetSocketAddress *) address);
                if(peer == NULL) {
                    continue;
                }

                g_hash_table_insert(peers_hash_table, &(peer->address_key), peer);
            }

            ring_buffer_input(peer->receive_ring_buffer, buffer, read);
        }

        // then, send data
        uint64_t total_elapsed_usec = (uint64_t)(g_timer_elapsed(timer, NULL) * 1000000);
        uint64_t target_num_samples_sent = (total_elapsed_usec * SAMPLE_RATE) / 1000000;
        uint64_t num_samples_missing = (target_num_samples_sent - total_num_samples_sent);
        //printf("%lu samples behind\n", num_samples_missing);

        unsigned int num_peers = g_hash_table_size(peers_hash_table);

        unsigned long num_samples = SAMPLES_PER_MESSAGE;
        while(num_samples_missing >= num_samples) {

            //printf("sending %lu samples each to %d peers\n", num_samples, num_peers);

            idle = false;

            size_t num_bytes = pcm_code_get_encoded_num_bytes_from_decoded_num_samples(num_samples);
            int16_t samples[num_peers * num_samples];
            peer_info *peer_infos[num_peers];

            GHashTableIter iter;
            peer_info *iter_peer;
            g_hash_table_iter_init(&iter, peers_hash_table);
            for(int i = 0; g_hash_table_iter_next(&iter, NULL, (void **) &iter_peer); i++) {
                peer_infos[i] = iter_peer;
                uint8_t buffer[num_bytes];
                ring_buffer_output(iter_peer->receive_ring_buffer, buffer, num_bytes);
                pcm_code_decode_bytes(buffer, num_bytes, &(samples[i * num_samples]));
            }

            for(unsigned int i = 0; i < num_peers; i++) {
                peer_info *peer = peer_infos[i];
                int16_t peer_samples[num_samples];
                pcm_code_combine_sample_blocks_except_index(samples, num_peers, num_samples, i, peer_samples);
                uint8_t peer_buffer[num_bytes];
                pcm_code_encode_samples(peer_samples, num_samples, peer_buffer);

                gssize written = g_socket_send_to(socket.gsocket, peer->socket_address, (gchar *) peer_buffer, num_bytes, NULL, NULL);

                (void) written;
                //printf("sent %lu bytes\n", written);
            }

            total_num_samples_sent += num_samples;
            num_samples_missing -= num_samples;

        }

        if((total_elapsed_usec / (1000L*1000)) > 24L*60*60) {
            // reset timer after some time to prevent losing accuracy
            printf("resetting timer\n");
            g_timer_start(timer);
            total_num_samples_sent = 0;
        }

        if(idle) {
            g_usleep(1000000L / SAMPLE_RATE);
        }

    }

    //

    return EXIT_SUCCESS;
}
