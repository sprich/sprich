#ifndef SPRICH_AUDIO_H
#define SPRICH_AUDIO_H

#include <stdbool.h>

#include "ring_buffer.h"
#include "audio_format.h"

typedef struct {
    uint8_t *input_buffer;
    uint8_t *output_buffer;
    size_t input_buffer_num_bytes;
    size_t input_buffer_num_samples;
    size_t input_buffer_num_frames;
    size_t output_buffer_num_bytes;
    size_t output_buffer_num_samples;
    size_t output_buffer_num_frames;
} audio_converter_args;

typedef void (*audio_converter_func)(void *state_ptr, audio_converter_args args);

typedef struct {
    audio_converter_func func;
    void *state_ptr;
} audio_converter;

typedef struct {
    const audio_format *source_format;
    ring_buffer *source_ring_buffer;
    GAsyncQueue *source_async_queue;
    const audio_format *target_format;
    ring_buffer *target_ring_buffer;
    GAsyncQueue *target_async_queue;
    GThread *thread;

    audio_converter converters[4];
    int num_converters;
} audio_conversion_info;

#ifdef SPRICH_OS_WIN
#include "audio_win.h"
#define SPRICH_AUDIO_SPECIFIC_RECORD_STREAM audio_win_stream
#define SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM audio_win_stream
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
#include "audio_linux.h"
#define SPRICH_AUDIO_SPECIFIC_RECORD_STREAM audio_linux_stream
#define SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM audio_linux_stream
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
#include "audio_macos.h"
#define SPRICH_AUDIO_SPECIFIC_RECORD_STREAM audio_macos_stream
#define SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM audio_macos_stream
#endif // #ifdef SPRICH_OS_MACOS

typedef struct {
    SPRICH_AUDIO_SPECIFIC_RECORD_STREAM *specific_record_stream;
    ring_buffer *ring_buffer;
    GAsyncQueue *async_queue;
    audio_conversion_info conversion_info;
} audio_record_stream;

typedef struct {
    SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM *specific_playback_stream;
    ring_buffer *ring_buffer;
    GAsyncQueue *async_queue;
    audio_conversion_info conversion_info;
} audio_playback_stream;

bool audio_init();
void audio_stop();

void audio_record_stream_free(audio_record_stream *audio_record_stream);
audio_record_stream *audio_record_stream_new();
audio_playback_stream *audio_playback_stream_new();

void audio_record_stream_free(audio_record_stream *audio_record_stream);
void audio_playback_stream_free(audio_playback_stream *audio_playback_stream);

#endif // #ifndef SPRICH_AUDIO_H
