#ifndef SPRICH_SOCKET_H
#define SPRICH_SOCKET_H

#include <stdbool.h>
#include <glib.h>
#include <gio/gio.h>

typedef struct {
    GSocket *gsocket;
} socket;

bool socket_init(socket *socket, gint local_port, bool blocking);

#endif // #ifndef SPRICH_SOCKET_H
