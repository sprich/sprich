#ifndef SPRICH_RING_BUFFER_H
#define SPRICH_RING_BUFFER_H

#include <stdbool.h>
#include <stdint.h>
#include <glib.h>

typedef struct {
    uint8_t *buffer;
    unsigned long length;
    unsigned long input_index;
    unsigned long output_index;
    unsigned long min_num_remaining_bytes;
    unsigned long num_remaining_bytes_counted_of_volume_bytes;
} ring_buffer;

bool ring_buffer_init(ring_buffer *ring_buffer);
ring_buffer *ring_buffer_new();
void ring_buffer_free(ring_buffer *ring_buffer);

void ring_buffer_input(ring_buffer *ring_buffer, const uint8_t *data, size_t num_bytes);
size_t ring_buffer_output_get_num_bytes_available(ring_buffer *ring_buffer);
void ring_buffer_output(ring_buffer *ring_buffer, uint8_t *target, size_t num_bytes);

#endif // #ifndef SPRICH_RING_BUFFER_H
