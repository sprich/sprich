
#include <stdio.h>
#include <stdint.h>

#include "constants.h"
#include "pcm_code.h"

void pcm_code_encode_samples(const int16_t *source_samples, unsigned long num_samples, uint8_t *target_bytes) {

    for(unsigned long sample_index = 0; sample_index < num_samples; sample_index++) {
        int16_t sample = source_samples[sample_index];
        for(uint8_t byte_index = 0; byte_index < SAMPLE_SIZE_BYTES; byte_index++) {
            target_bytes[(sample_index * SAMPLE_SIZE_BYTES) + byte_index] = ((sample >> (8 * byte_index)) & 0xFF);
        }
    }

}

void pcm_code_decode_bytes(const uint8_t *source_bytes, unsigned long num_bytes, int16_t *target_samples) {

    for(unsigned long global_byte_index = 0; global_byte_index < num_bytes; global_byte_index++) {
        uint8_t local_byte_index = global_byte_index % SAMPLE_SIZE_BYTES;
        unsigned long sample_index = global_byte_index / SAMPLE_SIZE_BYTES;

        if(local_byte_index == 0) {
            target_samples[sample_index] = 0;
        }

        uint8_t byte = source_bytes[global_byte_index];
        target_samples[sample_index] |= (((uint16_t) byte) << (8 * local_byte_index));
    }

}

void pcm_code_combine_sample_blocks_except_index(const int16_t *source_samples, unsigned long num_blocks, unsigned long num_samples_per_block, unsigned long exclude_block_index, int16_t *target_samples) {

    for(unsigned long sample_index = 0; sample_index < num_samples_per_block; sample_index++) {
        int32_t sample = 0;
        for(unsigned long block_index = 0; block_index < num_blocks; block_index++) {
            if(block_index != exclude_block_index) {
                sample += source_samples[(block_index * num_samples_per_block) + sample_index];
            }
        }

        if(sample > 0x7FFF) {
            sample = 0x7FFF;
        } else if(sample < -0x8000) {
            sample = -0x8000;
        }

        target_samples[sample_index] = sample;
    }

}
