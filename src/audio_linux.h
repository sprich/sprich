#ifndef SPRICH_AUDIO_LINUX_H
#define SPRICH_AUDIO_LINUX_H

#ifdef SPRICH_OS_LINUX

#include <glib.h>
#include <portaudio.h>

#include "ring_buffer.h"

typedef struct {
    ring_buffer *ring_buffer;
    GAsyncQueue *record_stream_async_queue;
    PaStream *pa_stream;
    audio_format format;
} audio_linux_stream;

#include "audio.h"

bool audio_linux_init();
audio_linux_stream *audio_linux_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *record_stream_async_queue);
audio_linux_stream *audio_linux_playback_stream_new(ring_buffer *ring_buffer);
void audio_linux_stream_free(audio_linux_stream *stream);
void audio_linux_stop();

#endif // #ifdef SPRICH_OS_LINUX

#endif // #ifndef SPRICH_AUDIO_LINUX_H
