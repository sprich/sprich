
void audio_linux_dummy() {
    // avoids an empty translation unit warning
}

#ifdef SPRICH_OS_LINUX

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <glib.h>
#include <portaudio.h>

#include "constants.h"
#include "audio.h"
#include "pcm_code.h"

void audio_linux_print_error_message(const char *error_message) {
    fprintf(stderr, "audio_linux: Error: %s\n", error_message);
}

static int input_callback(const void *input_buffer, void *output_buffer, unsigned long frames_per_buffer, const PaStreamCallbackTimeInfo *time_info, PaStreamCallbackFlags status_flags, void *user_data) {

    audio_linux_stream *stream = (audio_linux_stream *) user_data;

    (void) output_buffer; // ignore unused variable warning
    (void) time_info; // ignore unused variable warning
    (void) status_flags; // ignore unused variable warning

    const int16_t *sample_buffer = input_buffer;
    uint8_t byte_buffer[pcm_code_get_encoded_num_bytes_from_decoded_num_samples(frames_per_buffer)];

    ring_buffer *ring_buffer = stream->ring_buffer;
    GAsyncQueue *async_queue = stream->record_stream_async_queue;

    if(input_buffer != NULL && ring_buffer != NULL && async_queue != NULL) {

        pcm_code_encode_samples(sample_buffer, frames_per_buffer, byte_buffer);

        ring_buffer_input(ring_buffer, byte_buffer, SAMPLE_SIZE_BYTES * frames_per_buffer);

        g_async_queue_push(async_queue, stream);

    }

    return paContinue;
}

static int output_callback(const void *input_buffer, void *output_buffer, unsigned long frames_per_buffer, const PaStreamCallbackTimeInfo *time_info, PaStreamCallbackFlags status_flags, void *user_data) {

    audio_linux_stream *stream = (audio_linux_stream *) user_data;

    (void) input_buffer; // ignore unused variable warning
    (void) time_info; // ignore unused variable warning
    (void) status_flags; // ignore unused variable warning

    int16_t *sample_buffer = output_buffer;
    size_t num_bytes = pcm_code_get_encoded_num_bytes_from_decoded_num_samples(frames_per_buffer);
    uint8_t byte_buffer[num_bytes];

    ring_buffer *ring_buffer = stream->ring_buffer;

    if(output_buffer != NULL && ring_buffer != NULL) {

        ring_buffer_output(ring_buffer, byte_buffer, num_bytes);

        pcm_code_decode_bytes(byte_buffer, num_bytes, sample_buffer);

    }

    return paContinue;
}

void print_portaudio_error(PaError error) {
    fprintf(stderr, "PortAudio error %d: %s\n", error, Pa_GetErrorText(error));
}

bool audio_linux_init() {

    PaError pa_error;

    pa_error = Pa_Initialize();
    if(pa_error != paNoError) {
        print_portaudio_error(pa_error);
        return false;
    }

    int device_count = Pa_GetDeviceCount();
    printf("device count: %d\n", device_count);
    for(PaDeviceIndex i = 0; i < device_count; i++) {
        const PaDeviceInfo *device_info = Pa_GetDeviceInfo(i);
        const PaHostApiInfo *host_api_info = Pa_GetHostApiInfo(device_info->hostApi);
        printf("device index %d of %d: name=%s hostApi=%s\n", i, device_count, device_info->name, host_api_info->name);
    }

    return true;

}

void audio_linux_stop() {

    Pa_Terminate();

}

audio_linux_stream *audio_linux_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *record_stream_async_queue) {

    audio_linux_stream *stream = (audio_linux_stream *) malloc(sizeof(audio_linux_stream));
    if(stream == NULL) {
        audio_linux_print_error_message("Could not malloc audio_linux_stream");
        return NULL;
    }

    stream->ring_buffer = ring_buffer;
    stream->record_stream_async_queue = record_stream_async_queue;

    PaStreamParameters pa_stream_parameters;
    PaError pa_error;

    printf("getting default input device\n");

    pa_stream_parameters.device = Pa_GetDefaultInputDevice();
    if(pa_stream_parameters.device == paNoDevice) {
        audio_linux_print_error_message("No default input device");
        free(stream);
        return NULL;
    }

    {
        const PaDeviceInfo *device_info = Pa_GetDeviceInfo(pa_stream_parameters.device);
        const PaHostApiInfo *host_api_info = Pa_GetHostApiInfo(device_info->hostApi);
        printf("default input device name=%s hostApi=%s\n", device_info->name, host_api_info->name);
    }

    pa_stream_parameters.channelCount = 1;
    pa_stream_parameters.sampleFormat = paInt16;
    pa_stream_parameters.suggestedLatency = Pa_GetDeviceInfo(pa_stream_parameters.device)->defaultLowInputLatency;
    pa_stream_parameters.hostApiSpecificStreamInfo = NULL;

    PaStream *pa_stream;
    pa_error = Pa_OpenStream(&pa_stream, &pa_stream_parameters, NULL, 44100, 16, 0, input_callback, stream);
    if(pa_error != paNoError) {
        print_portaudio_error(pa_error);
        free(stream);
        return NULL;
    }

    pa_error = Pa_StartStream(pa_stream);
    if(pa_error != paNoError) {
        print_portaudio_error(pa_error);
        Pa_CloseStream(pa_stream);
        free(stream);
        return NULL;
    }

    stream->pa_stream = pa_stream;
    stream->format = (audio_format) {
        .sample_rate = 44100,
        .num_channels = 1,
        .num_bits_per_channel = 16,
        .is_float = false
    };

    return stream;

}

audio_linux_stream *audio_linux_playback_stream_new(ring_buffer *ring_buffer) {

    audio_linux_stream *stream = (audio_linux_stream *) malloc(sizeof(audio_linux_stream));
    if(stream == NULL) {
        audio_linux_print_error_message("Could not malloc audio_linux_stream");
        return NULL;
    }

    stream->ring_buffer = ring_buffer;
    stream->record_stream_async_queue = NULL;

    PaStreamParameters pa_stream_parameters;
    PaError pa_error;

    printf("getting default output device\n");

    pa_stream_parameters.device = Pa_GetDefaultOutputDevice();
    if(pa_stream_parameters.device == paNoDevice) {
        audio_linux_print_error_message("No default output device");
        return NULL;
    }

    {
        const PaDeviceInfo *device_info = Pa_GetDeviceInfo(pa_stream_parameters.device);
        const PaHostApiInfo *host_api_info = Pa_GetHostApiInfo(device_info->hostApi);
        printf("default output device name=%s hostApi=%s\n", device_info->name, host_api_info->name);
    }

    pa_stream_parameters.channelCount = 1;
    pa_stream_parameters.sampleFormat = paInt16;
    pa_stream_parameters.suggestedLatency = Pa_GetDeviceInfo(pa_stream_parameters.device)->defaultLowInputLatency;
    pa_stream_parameters.hostApiSpecificStreamInfo = NULL;

    PaStream *pa_stream;
    pa_error = Pa_OpenStream(&pa_stream, NULL, &pa_stream_parameters, 44100, 16, 0, output_callback, stream);
    if(pa_error != paNoError) {
        print_portaudio_error(pa_error);
        Pa_CloseStream(pa_stream);
        free(stream);
        return NULL;
    }

    pa_error = Pa_StartStream(pa_stream);
    if(pa_error != paNoError) {
        print_portaudio_error(pa_error);
        Pa_CloseStream(pa_stream);
        free(stream);
        return NULL;
    }

    stream->pa_stream = pa_stream;
    stream->format = (audio_format) {
        .sample_rate = 44100,
        .num_channels = 1,
        .num_bits_per_channel = 16,
        .is_float = false
    };

    return stream;

}

void audio_linux_stream_free(audio_linux_stream *stream) {

    PaStream *pa_stream = stream->pa_stream;
    if(pa_stream != NULL) {
        Pa_CloseStream(pa_stream);
    }

    free(stream);

}

#endif // #ifdef SPRICH_OS_LINUX
