
void audio_macos_dummy() {
    // avoids an empty translation unit warning
}

#ifdef SPRICH_OS_MACOS

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <glib.h>
#include <CoreServices/CoreServices.h>
#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>
#include <AudioUnit/AudioUnit.h>
#include <CoreAudio/CoreAudioTypes.h>
#include <CoreFoundation/CoreFoundation.h>

#include "constants.h"
#include "audio.h"
#include "pcm_code.h"

void audio_macos_print_error_message(const char *function_name, const char *error_message) {
    fprintf(stderr, "audio_macos: Error: %s: %s\n", function_name, error_message);
}

OSStatus audio_macos_input_callback(void *refCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *null_audio_buffer_list) {

    (void) ioActionFlags;
    (void) inTimeStamp;
    (void) null_audio_buffer_list;

    audio_macos_stream *stream = (audio_macos_stream *) refCon;

    //printf("audio_macos_input_callback: inNumberFrames = %d\n", inNumberFrames);

    OSStatus status = AudioUnitRender(stream->audio_unit, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, stream->audio_buffer_list);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_input_callback", "Could not AudioUnitRender");
        fprintf(stderr, "status=%ld\n", (long) status);
        return status;
    }

    /*printf("audio_macos_input_callback: stream->buffer=%p\n", stream->buffer);

    printf("audio_macos_input_callback: audio_buffer_list mNumberBuffers=%d\n", stream->audio_buffer_list->mNumberBuffers);
    for(int i = 0; i < stream->audio_buffer_list->mNumberBuffers; i++) {
        printf("audio_macos_input_callback: audio_buffer_list->mBuffers[%d] mNumberChannels=%d mDataByteSize=%d mData=%p\n", i, stream->audio_buffer_list->mBuffers[i].mNumberChannels, stream->audio_buffer_list->mBuffers[i].mDataByteSize, stream->audio_buffer_list->mBuffers[i].mData);
    }

    for(int i = 0; i < inNumberFrames; i++) {
        for(int j = 0; j < stream->audio_buffer_list->mNumberBuffers; j++) {
            printf("audio_macos_input_callback: sample %d channel %d = %f @ %p\n", i, j, ((float *)(stream->audio_buffer_list->mBuffers[j].mData))[i], (void *) &(((float *)(stream->audio_buffer_list->mBuffers[j].mData))[i]));
        }
    }*/

    ring_buffer_input(stream->ring_buffer, stream->buffer, inNumberFrames * stream->num_bytes_per_sample);
    g_async_queue_push(stream->async_queue, stream);

    return noErr;

}

OSStatus audio_macos_output_callback(void *refCon, AudioUnitRenderActionFlags *ioActionFlags, const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *audio_buffer_list) {

    (void) ioActionFlags;
    (void) inTimeStamp;
    (void) inBusNumber;
    (void) inNumberFrames;

    audio_macos_stream *stream = (audio_macos_stream *) refCon;

    //printf("audio_macos_output_callback: inBusNumber=%d inNumberFrames=%d audio_buffer_list=%p\n", inBusNumber, inNumberFrames, audio_buffer_list);

    size_t num_bytes = 0;

    if(audio_buffer_list != NULL) {
        //printf("audio_macos_output_callback: audio_buffer_list->mNumberBuffers=%d\n", audio_buffer_list->mNumberBuffers);
        for(uint32_t i = 0; i < audio_buffer_list->mNumberBuffers; i++) {
            //printf("audio_macos_output_callback: audio_buffer_list->mBuffers[%d]: mNumberChannels=%d mDataByteSize=%d mData=%p\n", i, audio_buffer_list->mBuffers[i].mNumberChannels, audio_buffer_list->mBuffers[i].mDataByteSize, audio_buffer_list->mBuffers[i].mData);
            if(audio_buffer_list->mBuffers[i].mDataByteSize > num_bytes) {
                num_bytes = audio_buffer_list->mBuffers[i].mDataByteSize;
            }
        }
    }

    uint8_t buffer[num_bytes];
    ring_buffer_output(stream->ring_buffer, buffer, num_bytes);

    if(audio_buffer_list != NULL) {
        for(uint32_t i = 0; i < audio_buffer_list->mNumberBuffers; i++) {
            memcpy(audio_buffer_list->mBuffers[i].mData, buffer, audio_buffer_list->mBuffers[i].mDataByteSize);
        }
    }

    return noErr;

}

bool audio_macos_init() {

    return true;

}

void audio_macos_stop() {

}

bool audio_macos_get_default_device_id(bool output, AudioDeviceID *device_id) {

    uint32_t size = sizeof(AudioDeviceID);

    AudioObjectPropertyAddress address = {output ? kAudioHardwarePropertyDefaultOutputDevice : kAudioHardwarePropertyDefaultInputDevice, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    OSStatus status = AudioObjectGetPropertyData(kAudioObjectSystemObject, &address, 0, NULL, &size, device_id);

    if(status != noErr) {
        fprintf(stderr, "audio_macos_get_default_device_id: Could not AudioObjectGetPropertyData, OSStatus=%ld\n", (long) status);
        return false;
    }

    return true;

}

bool audio_macos_get_device_sample_rate(AudioDeviceID device_id, double *sample_rate) {

    AudioObjectPropertyAddress sample_rate_address = {kAudioDevicePropertyNominalSampleRate, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster};
    uint32_t sample_rate_size = sizeof(*sample_rate);
    OSStatus status = AudioObjectGetPropertyData(device_id, &sample_rate_address, 0, NULL, &sample_rate_size, sample_rate);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_get_device_sample_rate", "Could not AudioObjectGetPropertyData to get the device sample rate");
        return false;
    }

    return true;

}

bool audio_macos_get_device_stream_basic_description(AudioDeviceID device_id, bool output, AudioStreamBasicDescription *audio_stream_basic_description) {

    AudioObjectPropertyAddress stream_format_property_address = {kAudioDevicePropertyStreamFormat, output ? kAudioDevicePropertyScopeOutput : kAudioDevicePropertyScopeInput, 0};
    uint32_t audio_stream_basic_description_size = sizeof(*audio_stream_basic_description);

    OSStatus status = AudioObjectGetPropertyData(device_id, &stream_format_property_address, 0, NULL, &audio_stream_basic_description_size, audio_stream_basic_description);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_get_device_stream_basic_description", "Could not AudioObjectGetPropertyData to get the device stream format");
        return false;
    }

    return true;

}

bool audio_macos_make_and_init_auhal(bool output, AudioDeviceID device_id, audio_macos_stream *stream, AudioUnit *audio_unit) {

    AudioComponentDescription audio_component_description;
    audio_component_description.componentType = kAudioUnitType_Output;
    audio_component_description.componentSubType = kAudioUnitSubType_HALOutput;
    audio_component_description.componentManufacturer = kAudioUnitManufacturer_Apple;
    audio_component_description.componentFlags = 0;
    audio_component_description.componentFlagsMask = 0;

    AudioComponent audio_component = AudioComponentFindNext(NULL, &audio_component_description);
    if(audio_component == NULL) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioComponentFindNext");
        return false;
    }

    *audio_unit = NULL;
    OSStatus status = AudioComponentInstanceNew(audio_component, audio_unit);
    if(status != noErr || *audio_unit == NULL) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioComponentInstanceNew");
        return false;
    }

    status = AudioUnitInitialize(*audio_unit);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "First AudioUnitInitialize failed");
        return false;
    }

    uint32_t enable_input = output ? 0 : 1;
    status = AudioUnitSetProperty(*audio_unit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, 1, &enable_input, sizeof(enable_input));
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioUnitSetProperty to enable/disable input");
        return false;
    }

    uint32_t enable_output = output ? 1 : 0;
    status = AudioUnitSetProperty(*audio_unit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, 0, &enable_output, sizeof(enable_output));
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioUnitSetProperty to enable/disable output");
        return false;
    }

    status = AudioUnitSetProperty(*audio_unit, kAudioOutputUnitProperty_CurrentDevice, kAudioUnitScope_Global, 0, &device_id, sizeof(device_id));
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioUnitSetProperty to set the device");
        return false;
    }

    AURenderCallbackStruct render_callback_struct;

    render_callback_struct.inputProc = output ? audio_macos_output_callback : audio_macos_input_callback;
    render_callback_struct.inputProcRefCon = stream;
    status = AudioUnitSetProperty(*audio_unit, output ? kAudioUnitProperty_SetRenderCallback : kAudioOutputUnitProperty_SetInputCallback, output ? kAudioUnitScope_Input : kAudioUnitScope_Global, 0, &render_callback_struct, sizeof(render_callback_struct));
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Could not AudioUnitSetProperty to set the callback");
        return false;
    }

    // TODO second AudioUnitInitialize necessary?
    status = AudioUnitInitialize(*audio_unit);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_and_init_auhal", "Final AudioUnitInitialize failed");
        return false;
    }

    return true;

}

bool audio_macos_make_audio_buffer_list(AudioUnit audio_unit, AudioStreamBasicDescription audio_stream_basic_description, AudioBufferList **audio_buffer_list, void **buffer, uint32_t *buffer_size_bytes) {

    uint32_t buffer_size_frames = 0;
    uint32_t buffer_size_frames_size = sizeof(buffer_size_frames);
    OSStatus status = AudioUnitGetProperty(audio_unit, kAudioDevicePropertyBufferFrameSize, kAudioUnitScope_Global, 0, &buffer_size_frames, &buffer_size_frames_size);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_make_audio_buffer_list", "Could not AudioUnitGetProperty to get buffer size");
        return false;
    }
    *buffer_size_bytes = buffer_size_frames * audio_stream_basic_description.mBytesPerFrame;

    size_t audio_buffer_list_size = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) * audio_stream_basic_description.mChannelsPerFrame);
    *audio_buffer_list = (AudioBufferList *) malloc(audio_buffer_list_size);
    if(*audio_buffer_list == NULL) {
        audio_macos_print_error_message("audio_macos_make_audio_buffer_list", "Could not malloc AudioBufferList");
        return false;
    }

    memset(*audio_buffer_list, 0, audio_buffer_list_size);

    (*audio_buffer_list)->mNumberBuffers = audio_stream_basic_description.mChannelsPerFrame;
    for(uint32_t i = 0; i < (*audio_buffer_list)->mNumberBuffers; i++) {
        void *current_buffer = malloc(*buffer_size_bytes);
        if(current_buffer == NULL) {
            audio_macos_print_error_message("audio_macos_make_audio_buffer_list", "Could not malloc buffer for AudioBuffer");
            return false;
        }
        (*audio_buffer_list)->mBuffers[i].mNumberChannels = 1;
        (*audio_buffer_list)->mBuffers[i].mDataByteSize = *buffer_size_bytes;
        (*audio_buffer_list)->mBuffers[i].mData = current_buffer;
    }

    *buffer = (*audio_buffer_list)->mBuffers[0].mData;

    return true;

}

bool audio_macos_start_audio_unit(AudioUnit audio_unit) {

    OSStatus status = AudioOutputUnitStart(audio_unit);
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_start_audio_unit", "Could not AudioOutputUnitStart to start stream");
        return false;
    }

    return true;

}

audio_macos_stream *audio_macos_stream_new(bool output, ring_buffer *ring_buffer, GAsyncQueue *async_queue) {

    audio_macos_stream *stream = NULL;
    AudioBufferList *audio_buffer_list = NULL;

    stream = (audio_macos_stream *) malloc(sizeof(audio_macos_stream));
    if(stream == NULL) {
        audio_macos_print_error_message("audio_macos_stream_new", "Could not malloc stream");
        goto abort;
    }

    stream->ring_buffer = ring_buffer;
    stream->async_queue = async_queue;
    stream->audio_unit = NULL;
    stream->audio_buffer_list = NULL;
    stream->buffer = NULL;
    stream->buffer_size_bytes = 0;
    stream->num_bytes_per_sample = 0;

    AudioDeviceID device_id;
    if(!audio_macos_get_default_device_id(output, &device_id)) {
        goto abort;
    }

    AudioStreamBasicDescription audio_stream_basic_description = {0};
    if(!audio_macos_get_device_stream_basic_description(device_id, output, &audio_stream_basic_description)) {
        goto abort;
    }

    printf("audio_macos: audio_stream_basic_description: mBitsPerChannel=%d mBytesPerFrame=%d mChannelsPerFrame=%d mFormatFlags&kAudioFormatFlagIsFloat=%d\n", audio_stream_basic_description.mBitsPerChannel, audio_stream_basic_description.mBytesPerFrame, audio_stream_basic_description.mChannelsPerFrame, (audio_stream_basic_description.mFormatFlags & kAudioFormatFlagIsFloat) != 0 ? 1 : 0);

    stream->format.num_channels = SPRICH_NUM_CHANNELS;
    stream->format.num_bits_per_channel = audio_stream_basic_description.mBitsPerChannel;
    stream->format.is_float = (audio_stream_basic_description.mFormatFlags & kAudioFormatFlagIsFloat) != 0;

    stream->num_bytes_per_sample = audio_stream_basic_description.mBitsPerChannel / 8;

    double sample_rate;
    if(!audio_macos_get_device_sample_rate(device_id, &sample_rate)) {
        goto abort;
    }

    printf("audio_macos: sample_rate=%f\n", sample_rate);

    stream->format.sample_rate = (int) sample_rate;

    AudioUnit audio_unit = NULL;
    if(!audio_macos_make_and_init_auhal(output, device_id, stream, &audio_unit)) {
        goto abort;
    }

    stream->audio_unit = audio_unit;

    if(!output) {
        void *buffer;
        uint32_t buffer_size_bytes;
        if(!audio_macos_make_audio_buffer_list(audio_unit, audio_stream_basic_description, &audio_buffer_list, &buffer, &buffer_size_bytes)) {
            goto abort;
        }

        stream->audio_buffer_list = audio_buffer_list;
        stream->buffer = buffer;
        stream->buffer_size_bytes = buffer_size_bytes;
    }

    if(!audio_macos_start_audio_unit(audio_unit)) {
        goto abort;
    }

    return stream;

    abort:
    if(audio_buffer_list != NULL) {
        for(uint32_t i = 0; i < audio_buffer_list->mNumberBuffers; i++) {
            void *buffer = audio_buffer_list->mBuffers[i].mData;
            if(buffer != NULL) {
                free(buffer);
            }
        }
        free(audio_buffer_list);
    }
    if(stream != NULL) {
        free(stream);
    }
    return NULL;

}

/*audio_macos_playback_stream *audio_macos_playback_stream_new(ring_buffer *ring_buffer) {

    audio_macos_playback_stream *playback_stream = (audio_macos_playback_stream *) malloc(sizeof(audio_macos_playback_stream));
    if(playback_stream == NULL) {
        audio_macos_print_error_message("audio_macos_playback_stream_new", "Could not malloc stream");
        goto abort;
    }

    playback_stream->ring_buffer = ring_buffer;

    AudioDeviceID device_id;
    if(!audio_macos_get_default_device_id(true, &device_id)) {
        goto abort;
    }

    AudioStreamBasicDescription audio_stream_basic_description = {0};
    if(!audio_macos_get_device_stream_basic_description(device_id, false, &audio_stream_basic_description)) {
        goto abort;
    }

    double sample_rate;
    if(!audio_macos_get_device_sample_rate(device_id)) {
        goto abort;
    }

    AudioUnit audio_unit = NULL;
    if(!audio_macos_make_and_init_auhal(true, &audio_unit)) {
        goto abort;
    }



    status = AudioUnitSetProperty(audio_unit, kAudioOutputUnitProperty_CurrentDevice, kAudioUnitScope_Global, 0, &device_id, sizeof(device_id));
    if(status != noErr) {
        audio_macos_print_error_message("audio_macos_playback_stream_new", "Could not AudioUnitSetProperty to set the device");
        goto abort;
    }

    return playback_stream;

    abort:
    if(playback_stream != NULL) {
        free(playback_stream);
    }
    return NULL;

}*/

audio_macos_stream *audio_macos_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *record_stream_async_queue) {

    return audio_macos_stream_new(false, ring_buffer, record_stream_async_queue);

}

audio_macos_stream *audio_macos_playback_stream_new(ring_buffer *ring_buffer) {

    return audio_macos_stream_new(true, ring_buffer, NULL);

}

void audio_macos_record_stream_free(audio_macos_stream *stream) {

    (void) stream;

}

void audio_macos_playback_stream_free(audio_macos_stream *stream) {

    (void) stream;

}

#endif // #ifdef SPRICH_OS_MACOS
