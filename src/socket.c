
#include <stdbool.h>
#include <stdio.h>
#include <glib.h>
#include <gio/gio.h>

#include "socket.h"

bool socket_init(socket *socket, gint local_port, bool blocking) {

    GError *gerror;

    GSocket *gsocket = g_socket_new(G_SOCKET_FAMILY_IPV4, G_SOCKET_TYPE_DATAGRAM, G_SOCKET_PROTOCOL_UDP, &gerror);
    if(gsocket == NULL) {
        fprintf(stderr, "Could not create socket: %s\n", gerror->message);
        g_error_free(gerror);
        return false;
    }

    g_socket_set_blocking(gsocket, blocking);

    GSocketAddress *local_socket_address = g_inet_socket_address_new_from_string("0.0.0.0", local_port);
    if(local_socket_address == NULL) {
        fprintf(stderr, "Could not create local socket address\n");
        return false;
    }

    gboolean success = g_socket_bind(gsocket, local_socket_address, false, &gerror);
    if(!success) {
        fprintf(stderr, "Could not bind socket: %s\n", gerror->message);
        g_error_free(gerror);
        return false;
    }

    socket->gsocket = gsocket;

    return true;

}
