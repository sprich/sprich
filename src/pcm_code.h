#ifndef SPRICH_PCM_CODE_H
#define SPRICH_PCM_CODE_H

#include <stdint.h>

#include "constants.h"

#define pcm_code_get_encoded_num_bytes_from_decoded_num_samples(decoded_num_samples) (decoded_num_samples * SAMPLE_SIZE_BYTES)

void pcm_code_encode_samples(const int16_t *source_samples, unsigned long num_samples, uint8_t *target_bytes);
void pcm_code_decode_bytes(const uint8_t *source_bytes, unsigned long num_bytes, int16_t *target_samples);

void pcm_code_combine_sample_blocks_except_index(const int16_t *source_samples, unsigned long num_blocks, unsigned long num_samples_per_block, unsigned long exclude_block_index, int16_t *target_samples);

#endif // #ifndef SPRICH_PCM_CODE_H
