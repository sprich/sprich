
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ring_buffer.h"
#include "constants.h"

bool ring_buffer_init(ring_buffer *ring_buffer) {

    ring_buffer->buffer = malloc(RING_BUFFER_SIZE_BYTES);
    if(ring_buffer->buffer == NULL) {
        return false;
    }

    ring_buffer->length = RING_BUFFER_SIZE_BYTES;
    ring_buffer->input_index = 0;
    ring_buffer->output_index = 0;
    ring_buffer->min_num_remaining_bytes = 0;
    ring_buffer->num_remaining_bytes_counted_of_volume_bytes = 0;

    return true;

}

ring_buffer *ring_buffer_new() {

    ring_buffer *buffer = malloc(sizeof(ring_buffer));
    if(buffer == NULL) {
        return NULL;
    }

    if(!ring_buffer_init(buffer)) {
        free(buffer);
        return NULL;
    }

    return buffer;

}

void ring_buffer_free(ring_buffer *ring_buffer) {

    if(ring_buffer != NULL) {

        free(ring_buffer->buffer);
        ring_buffer->buffer = NULL;

        free(ring_buffer);

    }

}

void ring_buffer_input(ring_buffer *ring_buffer, const uint8_t *data, size_t num_bytes) {

    //printf("inputting %lu bytes\n", num_bytes);

    size_t copied_bytes = 0;
    while(copied_bytes < num_bytes) {
        size_t copy_num_bytes = num_bytes - copied_bytes;
        size_t num_bytes_til_end = (ring_buffer->length - ring_buffer->input_index);
        if(copy_num_bytes > num_bytes_til_end) {
            copy_num_bytes = num_bytes_til_end;
        }

        memcpy(ring_buffer->buffer + ring_buffer->input_index, data + copied_bytes, copy_num_bytes);
        ring_buffer->input_index += copy_num_bytes;
        ring_buffer->input_index %= ring_buffer->length;

        copied_bytes += copy_num_bytes;
    }

}

size_t ring_buffer_output_get_num_bytes_available(ring_buffer *ring_buffer) {

    size_t num_bytes_available;
    if(ring_buffer->output_index <= ring_buffer->input_index) {
        num_bytes_available = (ring_buffer->input_index - ring_buffer->output_index);
    } else {
        num_bytes_available = (ring_buffer->length - ring_buffer->output_index + ring_buffer->input_index);
    }

    return num_bytes_available;

}



void ring_buffer_output_internal(ring_buffer *ring_buffer, uint8_t *target, size_t num_bytes) {

    size_t copied_bytes = 0;
    while(copied_bytes < num_bytes) {
        size_t num_bytes_available = ring_buffer_output_get_num_bytes_available(ring_buffer);
        size_t num_bytes_til_end = (ring_buffer->length - ring_buffer->output_index);
        if(num_bytes_available > num_bytes_til_end) {
            num_bytes_available = num_bytes_til_end;
        }

        size_t copy_num_bytes = num_bytes - copied_bytes;

        if(num_bytes_available == 0) {
            //printf("... using silence for the last %lu bytes\n", (unsigned long) copy_num_bytes);
            if(target != NULL) {
                memset(target + copied_bytes, 0, copy_num_bytes);
            }
            break;
        }

        if(copy_num_bytes > num_bytes_available) {
            copy_num_bytes = num_bytes_available;
        }

        if(target != NULL) {
            memcpy(target + copied_bytes, ring_buffer->buffer + ring_buffer->output_index, copy_num_bytes);
        }
        ring_buffer->output_index += copy_num_bytes;
        ring_buffer->output_index %= ring_buffer->length;

        copied_bytes += copy_num_bytes;
    }

}

void ring_buffer_output(ring_buffer *ring_buffer, uint8_t *target, size_t num_bytes) {

    //printf("outputting %lu bytes\n", num_bytes);

    size_t available_bytes = ring_buffer_output_get_num_bytes_available(ring_buffer);

    //printf("%lu frames available for output, %lu requested\n", available_frames, frames_per_buffer);

    unsigned long remaining_bytes = 0;
    if(available_bytes > num_bytes) {
        remaining_bytes = (available_bytes - num_bytes);
    }

    if(remaining_bytes < ring_buffer->min_num_remaining_bytes || ring_buffer->num_remaining_bytes_counted_of_volume_bytes == 0) {
        ring_buffer->min_num_remaining_bytes = remaining_bytes;
    }

    ring_buffer->num_remaining_bytes_counted_of_volume_bytes += num_bytes;

    if(ring_buffer->num_remaining_bytes_counted_of_volume_bytes >= RING_BUFFER_SKIP_COUNT_NUM_REMAINING_BYTES_OF_VOLUME_BYTES) {
        //printf("counted num remaining bytes values over a volume of %lu bytes, min=%lu\n", ring_buffer->num_remaining_bytes_counted_of_volume_bytes, ring_buffer->min_num_remaining_bytes);

        if(ring_buffer->min_num_remaining_bytes > RING_BUFFER_SKIP_MIN_MIN_REMAINING_BYTES) {
            unsigned long num_skip_bytes = (ring_buffer->min_num_remaining_bytes / 2);
            num_skip_bytes -= (num_skip_bytes % RING_BUFFER_SKIP_MULTIPLE_OF_BYTES);
            printf("skipping %ld bytes to catch up\n", num_skip_bytes);
            ring_buffer_output_internal(ring_buffer, NULL, num_skip_bytes);
        }

        ring_buffer->num_remaining_bytes_counted_of_volume_bytes = 0;
        ring_buffer->min_num_remaining_bytes = 0;
    }

    ring_buffer_output_internal(ring_buffer, target, num_bytes);

}
