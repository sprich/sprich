
#include "audio_format.h"

audio_format audio_format_default = {
    .sample_rate = SPRICH_FRAME_RATE,
    .num_channels = SPRICH_NUM_CHANNELS,
    .num_bits_per_channel = SPRICH_SAMPLE_SIZE_BITS,
    .is_float = SPRICH_SAMPLE_IS_FLOAT
};

audio_format *audio_format_get_default() {
    return &audio_format_default;
}

bool audio_format_equal(audio_format format_a, audio_format format_b) {
    return format_a.is_float == format_b.is_float && format_a.sample_rate == format_b.sample_rate && format_a.num_channels == format_b.num_channels && format_a.num_bits_per_channel == format_b.num_bits_per_channel;
}

bool audio_format_needs_conversion(audio_format format) {
    return !audio_format_equal(format, audio_format_default);
}

bool audio_format_validate_for_conversion(audio_format format) {

    if((format.num_bits_per_channel % 8) != 0) {
        fprintf(stderr, "audio: audio_format_validate_for_conversion: unsupported format.num_bits_per_channel %d (not multiple of 8)\n", format.num_bits_per_channel);
        return false;
    }

    if(format.is_float && format.num_bits_per_channel != 32 && format.num_bits_per_channel != 64) {
        fprintf(stderr, "audio: audio_format_validate_for_conversion: unsupported format.num_bits_per_channel %d in conjunction with format.is_float true\n", format.num_bits_per_channel);
        return false;
    }

    return true;

}

size_t audio_format_get_frame_size_num_bytes(audio_format format) {

    return ((((size_t) format.num_bits_per_channel) + 7)/8) * format.num_channels;

}
