
#ifdef SPRICH_OS_WIN

#include <cstdio>

#include <initguid.h>
#include <objbase.h>
#include <audioclient.h>
#include <audiopolicy.h>
#include <mmdeviceapi.h>
#include <audiosessiontypes.h>
#include <processthreadsapi.h>

#include <glib.h>

extern "C" {
#include "constants.h"
#include "pcm_code.h"
}

#ifndef AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM
#define AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM 0x80000000
#endif // #ifndef AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM

#ifndef AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY
#define AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY 0x08000000
#endif // #ifndef AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY

#include "audio_win.h"

typedef struct {
    IAudioClient *audio_client;
    IAudioRenderClient *audio_render_client;
    IAudioCaptureClient *audio_capture_client;
    ring_buffer *ring_buffer;
    GAsyncQueue *async_queue;
    HANDLE initialized_event_handle;
    UINT32 frame_num_bytes;
    UINT32 buffer_num_frames;
    bool playback;
    bool initialized;
} audio_win_stream_info;

bool audio_win_playback_stream_write(audio_win_stream_info *stream_info, uint8_t *pcm_coding_buffer) {

    UINT32 padding_num_frames;
    HRESULT hresult = stream_info->audio_client->GetCurrentPadding(&padding_num_frames);
    if(FAILED(hresult)) {
        fprintf(stderr, "audio_win_playback_stream_write: Could not GetCurrentPadding, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    UINT32 num_frames_available = stream_info->buffer_num_frames - padding_num_frames;

    BYTE *sample_buffer;

    hresult = stream_info->audio_render_client->GetBuffer(num_frames_available, &sample_buffer);
    if(FAILED(hresult)) {
        fprintf(stderr, "audio_win_playback_stream_write: Could not GetBuffer, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    UINT32 num_bytes = stream_info->frame_num_bytes * num_frames_available;

    ring_buffer_output(stream_info->ring_buffer, pcm_coding_buffer, num_bytes);

    pcm_code_decode_bytes(pcm_coding_buffer, num_bytes, (int16_t *) sample_buffer);

    hresult = stream_info->audio_render_client->ReleaseBuffer(num_frames_available, 0);
    if(FAILED(hresult)) {
        fprintf(stderr, "audio_win_playback_stream_write: Could not ReleaseBuffer, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    return true;

}

bool audio_win_record_stream_read(audio_win_stream_info *stream_info, uint8_t *pcm_coding_buffer) {

    BYTE *sample_buffer;
    UINT32 num_frames_available = 0;
    DWORD flags;

    HRESULT hresult = stream_info->audio_capture_client->GetBuffer(&sample_buffer, &num_frames_available, &flags, NULL, NULL);
    if(FAILED(hresult)) {
        fprintf(stderr, "audio_win_record_stream_read: Could not GetBuffer, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    pcm_code_encode_samples((int16_t *) sample_buffer, num_frames_available, pcm_coding_buffer);

    UINT32 num_bytes = stream_info->frame_num_bytes * num_frames_available;
    ring_buffer_input(stream_info->ring_buffer, pcm_coding_buffer, num_bytes);

    hresult = stream_info->audio_capture_client->ReleaseBuffer(num_frames_available);
    if(FAILED(hresult)) {
        fprintf(stderr, "audio_win_record_stream_read: Could not ReleaseBuffer, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    GAsyncQueue *async_queue = stream_info->async_queue;
    if(async_queue != NULL) {
        g_async_queue_push(async_queue, stream_info);
    }

    return true;

}

DWORD audio_win_stream_thread_main(LPVOID data) {

    audio_win_stream_info *stream_info = (audio_win_stream_info *) data;

    bool (*data_transfer_callback)(audio_win_stream_info *stream_info, uint8_t *pcm_coding_buffer) = (stream_info->playback ? audio_win_playback_stream_write : audio_win_record_stream_read);

    DWORD exit_status = EXIT_FAILURE;

    IMMDeviceEnumerator *device_enumerator = NULL;
    IMMDevice *device = NULL;
    HANDLE event_handle = NULL;
    uint8_t *pcm_coding_buffer = NULL;

    stream_info->audio_client = NULL;
    stream_info->audio_render_client = NULL;

    {

        HRESULT hresult = CoInitialize(NULL);
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not CoInitialize, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        hresult = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void **) &device_enumerator);
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not create MMDeviceEnumerator, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        hresult = device_enumerator->GetDefaultAudioEndpoint(stream_info->playback ? eRender : eCapture, eCommunications, &device);
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not GetDefaultAudioEndpoint, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        hresult = device->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**) &(stream_info->audio_client));
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not Activate IMMDevice, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        WAVEFORMATEX waveFormatEx;
        waveFormatEx.wFormatTag = WAVE_FORMAT_PCM;
        waveFormatEx.nChannels = SPRICH_NUM_CHANNELS;
        waveFormatEx.nSamplesPerSec = SPRICH_FRAME_RATE;
        waveFormatEx.nAvgBytesPerSec = SPRICH_FRAME_RATE * SPRICH_FRAME_SIZE_BYTES;
        waveFormatEx.nBlockAlign = SPRICH_FRAME_SIZE_BYTES;
        waveFormatEx.wBitsPerSample = SPRICH_SAMPLE_SIZE_BITS;
        waveFormatEx.cbSize = 0;

        hresult = stream_info->audio_client->Initialize(AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_EVENTCALLBACK|AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM|AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY, 0, 0, &waveFormatEx, NULL);
        if(FAILED(hresult)) {
            if(hresult == AUDCLNT_E_UNSUPPORTED_FORMAT) {
                fprintf(stderr, "Could not Initialize IAudioClient, hresult=0x%x (AUDCLNT_E_UNSUPPORTED_FORMAT)\n", (unsigned int) hresult);
            } else {
                fprintf(stderr, "Could not Initialize IAudioClient, hresult=0x%x\n", (unsigned int) hresult);
            }
            goto exit;
        }

        stream_info->frame_num_bytes = waveFormatEx.nBlockAlign;

        event_handle = CreateEvent(NULL, FALSE, FALSE, NULL);
        if(event_handle == NULL) {
            fprintf(stderr, "Could not CreateEvent, GetLastError()=0x%x\n", (unsigned int) GetLastError());
            goto exit;
        }

        hresult = stream_info->audio_client->SetEventHandle(event_handle);
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not SetEventHandle on IAudioClient, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        hresult = stream_info->audio_client->GetBufferSize(&(stream_info->buffer_num_frames));
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not GetBufferSize on IAudioClient, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        pcm_coding_buffer = (uint8_t *) malloc(stream_info->frame_num_bytes * stream_info->buffer_num_frames);
        if(pcm_coding_buffer == NULL) {
            fprintf(stderr, "Could not malloc pcm_coding_buffer\n");
            goto exit;
        }

        if(stream_info->playback) {

            hresult = stream_info->audio_client->GetService(IID_IAudioRenderClient, (void **) &(stream_info->audio_render_client));
            if(FAILED(hresult)) {
                fprintf(stderr, "Could not get IAudioRenderClient from IAudioClient, hresult=0x%x\n", (unsigned int) hresult);
                goto exit;
            }

            bool success = data_transfer_callback(stream_info, pcm_coding_buffer);
            if(!success) {
                fprintf(stderr, "Could not write initial buffer\n");
                goto exit;
            }

        } else {

            hresult = stream_info->audio_client->GetService(IID_IAudioCaptureClient, (void **) &(stream_info->audio_capture_client));
            if(FAILED(hresult)) {
                fprintf(stderr, "Could not get IAudioCaptureClient from IAudioClient, hresult=0x%x\n", (unsigned int) hresult);
                goto exit;
            }

        }

        hresult = stream_info->audio_client->Start();
        if(FAILED(hresult)) {
            fprintf(stderr, "Could not Start, hresult=0x%x\n", (unsigned int) hresult);
            goto exit;
        }

        stream_info->initialized = true;
        SetEvent(stream_info->initialized_event_handle);

        for(;;) {

            DWORD result = WaitForSingleObject(event_handle, INFINITE);
            switch(result) {
                case WAIT_OBJECT_0: {
                    break;
                }
                case WAIT_FAILED: {
                    fprintf(stderr, "audio_win_stream_thread_main: Could not WaitForSingleObject (result=WAIT_FAILED), GetLastError()=0x%x\n", (unsigned int) GetLastError());
                    goto exit;
                }
                default: {
                    fprintf(stderr, "audio_win_stream_thread_main: Could not WaitForSingleObject, result=0x%x\n", (unsigned int) result);
                    goto exit;
                }
            }

            bool success = data_transfer_callback(stream_info, pcm_coding_buffer);
            if(!success) {
                goto exit;
            }

        }

    }

    exit:

    if(!stream_info->initialized) {
        SetEvent(stream_info->initialized_event_handle);
    }

    if(stream_info->audio_render_client != NULL) {
        stream_info->audio_render_client->Release();
    }
    if(pcm_coding_buffer != NULL) {
        free(pcm_coding_buffer);
    }
    if(event_handle != NULL) {
        CloseHandle(event_handle);
    }
    if(stream_info->audio_client != NULL) {
        stream_info->audio_client->Release();
    }
    if(device != NULL) {
        device->Release();
    }
    if(device_enumerator != NULL) {
        device_enumerator->Release();
    }

    return exit_status;

}

bool audio_win_init() {

    HRESULT hresult = CoInitialize(NULL);
    if(FAILED(hresult)) {
        fprintf(stderr, "Could not CoInitialize, hresult=0x%x\n", (unsigned int) hresult);
        return false;
    }

    return true;

}

audio_win_stream *stream_new(ring_buffer *ring_buffer, GAsyncQueue *async_queue, bool playback) {

    HANDLE initialized_event_handle = NULL;
    audio_win_stream *stream = NULL;
    audio_win_stream_info *stream_info = NULL;

    {

        initialized_event_handle = CreateEvent(NULL, FALSE, FALSE, NULL);
        if(initialized_event_handle == NULL) {
            fprintf(stderr, "Could not CreateEvent, GetLastError()=0x%x\n", (unsigned int) GetLastError());
            goto exit;
        }

        stream = (audio_win_stream *) malloc(sizeof(audio_win_stream));
        if(stream == NULL) {
            fprintf(stderr, "Could not create audio_win_stream\n");
            goto exit;
        }

        stream_info = (audio_win_stream_info *) malloc(sizeof(audio_win_stream_info));
        if(stream_info == NULL) {
            fprintf(stderr, "Could not create audio_win_stream_info\n");
            goto exit;
        }

        stream_info->audio_client = NULL;
        stream_info->audio_render_client = NULL;
        stream_info->audio_capture_client = NULL;
        stream_info->ring_buffer = ring_buffer;
        stream_info->async_queue = async_queue;
        stream_info->initialized_event_handle = initialized_event_handle;
        stream_info->frame_num_bytes = 0;
        stream_info->buffer_num_frames = 0;
        stream_info->playback = playback;
        stream_info->initialized = false;

        audio_format format;
        format.num_channels = SPRICH_NUM_CHANNELS;
        format.num_bits_per_channel = SPRICH_SAMPLE_SIZE_BITS;
        format.sample_rate = SPRICH_FRAME_RATE;
        format.is_float = false;

        stream->internal_data = stream_info;
        stream->format = format;

        DWORD thread_id;
        HANDLE thread_handle = CreateThread(NULL, 0, audio_win_stream_thread_main, stream_info, 0, &thread_id);
        if(thread_handle == NULL) {
            fprintf(stderr, "Could not CreateThread\n");
            goto exit;
        }

        DWORD result = WaitForSingleObject(stream_info->initialized_event_handle, INFINITE);
        switch(result) {
            case WAIT_OBJECT_0: {
                if(stream_info->initialized) {
                    return stream;
                } else {
                    fprintf(stderr, "Thread initialization failed\n");
                    goto exit;
                }
            }
            case WAIT_FAILED: {
                fprintf(stderr, "stream_new: Could not WaitForSingleObject (result=WAIT_FAILED), GetLastError()=0x%x\n", (unsigned int) GetLastError());
                goto exit;
            }
            default: {
                fprintf(stderr, "stream_new: Could not WaitForSingleObject, result=0x%x\n", (unsigned int) result);
                goto exit;
            }
        }

    }

    exit:

    free(stream_info);
    free(stream);
    if(initialized_event_handle != NULL) {
        CloseHandle(initialized_event_handle);
    }

    return NULL;

}

audio_win_stream *audio_win_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *async_queue) {
    return stream_new(ring_buffer, async_queue, false);
}

audio_win_stream *audio_win_playback_stream_new(ring_buffer *ring_buffer) {
    return stream_new(ring_buffer, NULL, true);
}

void audio_win_stream_free(audio_win_stream *stream) {
    (void) stream;
    // TODO
}

void audio_win_stop() {
    // TODO
}

#endif // #ifdef SPRICH_OS_WIN
