#ifndef SPRICH_AUDIO_FORMAT_H
#define SPRICH_AUDIO_FORMAT_H

#include <stdbool.h>
#include <stdio.h>

#include "constants.h"

typedef struct {
    int sample_rate;
    int num_channels;
    int num_bits_per_channel;
    bool is_float;
} audio_format;

audio_format *audio_format_get_default();
bool audio_format_equal(audio_format format_a, audio_format format_b);
bool audio_format_needs_conversion(audio_format format);
bool audio_format_validate_for_conversion(audio_format format);
size_t audio_format_get_frame_size_num_bytes(audio_format format);

#endif // #ifndef SPRICH_AUDIO_FORMAT_H
