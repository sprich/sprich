
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <gio/gio.h>
#include <stdbool.h>

#include "constants.h"
#include "ring_buffer.h"
#include "socket.h"
#include "audio.h"

typedef struct {
    audio_record_stream *audio_record_stream;
    socket *local_socket;
    GSocketAddress *remote_socket_address;
} record_stream_info;

typedef struct {
    audio_playback_stream *audio_playback_stream;
    socket *local_socket;
} playback_stream_info;

gpointer send_thread_main(gpointer data) {

    record_stream_info *info = (record_stream_info *) data;

    uint8_t buffer[0x10000];

    for(;;) {

        gpointer data = g_async_queue_pop(info->audio_record_stream->async_queue);
        (void) data;

        size_t available;
        while((available = ring_buffer_output_get_num_bytes_available(info->audio_record_stream->ring_buffer)) > 0) {
            if(available > sizeof(buffer)) {
                available = sizeof(buffer);
            }

            ring_buffer_output(info->audio_record_stream->ring_buffer, buffer, available);
            gssize written = g_socket_send_to(info->local_socket->gsocket, info->remote_socket_address, (gchar *) buffer, available, NULL, NULL);

            (void) written;
            //printf("sent %lu of %lu available bytes\n", written, available);
        }

    }

    return NULL;

}

gpointer receive_thread_main(gpointer data) {

    playback_stream_info *info = (playback_stream_info *) data;

    uint8_t buffer[0x10000];

    for(;;) {

        gssize read = g_socket_receive(info->local_socket->gsocket, (gchar *) buffer, sizeof(buffer), NULL, NULL);
        //printf("received %ld bytes\n", read);

        if(read < 0) {
            continue;
        }

        ring_buffer_input(info->audio_playback_stream->ring_buffer, buffer, read);

        GAsyncQueue *async_queue = info->audio_playback_stream->async_queue;
        if(async_queue != NULL) {
            g_async_queue_push(async_queue, async_queue);
        }

    }

    return NULL;

}

static gint local_port = 1234;
static gchar *remote_ip = "127.0.0.1";
static gint remote_port = 1234;

static GOptionEntry entries[] = {
    {"local-port", 0, 0, G_OPTION_ARG_INT, &local_port, "Local port to bind to", NULL},
    {"remote-ip", 0, 0, G_OPTION_ARG_STRING, &remote_ip, "Remote IP address", NULL},
    {"remote-port", 0, 0, G_OPTION_ARG_INT, &remote_port, "Remote port", NULL},
    {NULL}
};

int main(int argc, char **argv) {
    (void) argc;

    bool success;

    GError *error;

    GOptionContext *context;
    gchar **args;

#ifdef G_OS_WIN32
    args = g_win32_get_command_line ();
    (void) argv;
#else
    args = g_strdupv(argv);
#endif

    context = g_option_context_new(NULL);
    g_option_context_add_main_entries(context, entries, NULL);

    if(!g_option_context_parse_strv(context, &args, &error)) {
        fprintf(stderr, "%s\n", error->message);
        return EXIT_FAILURE;
    }

    g_strfreev(args);

    printf("local_port=%d\n", local_port);
    printf("remote_ip=%s\n", remote_ip);
    printf("remote_port=%d\n", remote_port);

    socket socket;

    success = socket_init(&socket, local_port, true);
    if(!success) {
        return EXIT_FAILURE;
    }

    GSocketAddress *remote_socket_address = g_inet_socket_address_new_from_string(remote_ip, remote_port);
    if(remote_socket_address == NULL) {
        fprintf(stderr, "Could not create remote socket address\n");
        return EXIT_FAILURE;
    }

    //

    success = audio_init();
    if(!success) {
        return EXIT_FAILURE;
    }

    audio_record_stream *audio_record_stream = audio_record_stream_new();
    if(audio_record_stream == NULL) {
        return EXIT_FAILURE;
    }

    audio_playback_stream *audio_playback_stream = audio_playback_stream_new();
    if(audio_playback_stream == NULL) {
        return EXIT_FAILURE;
    }

    record_stream_info record_stream_info;
    record_stream_info.audio_record_stream = audio_record_stream;
    record_stream_info.local_socket = &socket;
    record_stream_info.remote_socket_address = remote_socket_address;

    playback_stream_info playback_stream_info;
    playback_stream_info.audio_playback_stream = audio_playback_stream;
    playback_stream_info.local_socket = &socket;

    //

    GThread *send_thread = g_thread_new("send", send_thread_main, &record_stream_info);
    (void) send_thread;

    GThread *receive_thread = g_thread_new("receive", receive_thread_main, &playback_stream_info);
    (void) receive_thread;

    //

    getchar();

    audio_stop();

    return EXIT_SUCCESS;
}
