
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <glib.h>
#include <math.h>

#include "audio.h"
#include "pcm_code.h"
#include "audio_format.h"

#include "audio_win.h"
#include "audio_linux.h"

typedef audio_converter (*converter_constructor_func)(audio_format *source_format, audio_format target_format);

typedef struct {
    long source_frame_rate;
    long target_frame_rate;
    int num_channels;
} resample_converter_state;

void print_error_message(const char *error_message) {
    fprintf(stderr, "audio: Error: %s\n", error_message);
}

bool audio_init() {

#ifdef SPRICH_OS_WIN
    bool success = audio_win_init();
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
    bool success = audio_linux_init();
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
    bool success = audio_macos_init();
#endif // #ifdef SPRICH_OS_MACOS

    return success;

}

void audio_stop() {

#ifdef SPRICH_OS_WIN
    audio_win_stop();
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
    audio_linux_stop();
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
    audio_macos_stop();
#endif // #ifdef SPRICH_OS_MACOS

}

void swap(uint8_t **a, uint8_t **b) {
    uint8_t *temp = *a;
    *a = *b;
    *b = temp;
}

bool add_converter(converter_constructor_func constructor_func, audio_format *source_format, audio_format target_format, audio_converter *converters, int *num_converters, int max_num_converters) {

    if(*num_converters >= max_num_converters) {
        fprintf(stderr, "audio: add_converter: converters array full\n");
        return false;
    }

    audio_converter new_converter = constructor_func(source_format, target_format);

    if(new_converter.func == NULL) {
        return false;
    }

    converters[*num_converters] = new_converter;
    (*num_converters)++;

    return true;

}

void converter_func_float_to_int(void *state_ptr, audio_converter_args args) {
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        float value = ((float *)(args.input_buffer))[i];
        if(fabs(value) > *((float *) state_ptr)) {
            *((float *) state_ptr) = (float) fabs(value);
            //printf("converter_func_float_to_int: new max: %f\n", *((float *) state_ptr));
        }
        value /= *((float *) state_ptr);
        ((int32_t *)(args.output_buffer))[i] = (int32_t)(0x7FFFFFFF * value);
    }
}

void converter_func_double_to_long(void *state_ptr, audio_converter_args args) {
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        double value = ((double *)(args.input_buffer))[i];
        if(fabs(value) > *((double *) state_ptr)) {
            *((double *) state_ptr) = fabs(value);
            //printf("converter_func_double_to_long: new max: %f\n", *((double *) state_ptr));
        }
        value /= *((double *) state_ptr);
        ((int64_t *)(args.output_buffer))[i] = (int64_t)(0x7FFFFFFFFFFFFFFFL * value);
    }
}

void converter_func_int_to_float(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((float *)(args.output_buffer))[i] = (((int32_t *)(args.input_buffer))[i] / ((float) 0x7FFFFFFF));
    }
}

void converter_func_long_to_double(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((double *)(args.output_buffer))[i] = (((int64_t *)(args.input_buffer))[i] / ((double) 0x7FFFFFFFFFFFFFFF));
    }
}

audio_converter converter_constructor_cast(audio_format *source_format, audio_format target_format) {

    audio_converter c = {
        .func = NULL,
        .state_ptr = NULL
    };

    if(source_format->is_float && !target_format.is_float) {
        if(source_format->num_bits_per_channel == 32) {
            c.state_ptr = malloc(sizeof(float));
            if(c.state_ptr != NULL) {
                *((float *)(c.state_ptr)) = 1.0;
                c.func = converter_func_float_to_int;
            }
        } else if(source_format->num_bits_per_channel == 64) {
            c.state_ptr = malloc(sizeof(double));
            if(c.state_ptr != NULL) {
                *((double *)(c.state_ptr)) = 1.0;
                c.func = converter_func_double_to_long;
            }
        }
    } else if(!source_format->is_float && target_format.is_float) {
        if(source_format->num_bits_per_channel == 32) {
            c.func = converter_func_int_to_float;
        } else if(source_format->num_bits_per_channel == 64) {
            c.func = converter_func_long_to_double;
        }
    }

    source_format->is_float = target_format.is_float;

    return c;

}

void converter_func_stretch_float_double(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((double *)(args.output_buffer))[i] = ((double)(((float *)(args.input_buffer))[i]));
    }
}

void converter_func_stretch_int16_int32(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        int32_t in_value = ((int32_t)(((int16_t *)(args.input_buffer))[i]));
        ((int32_t *)(args.output_buffer))[i] = (((int32_t)(((int16_t *)(args.input_buffer))[i])) << 16);
        int32_t out_value = ((int32_t *)(args.output_buffer))[i];
        printf("converter_func_stretch_int16_int32: %12d -> %12d\n", in_value, out_value);
    }
}

void converter_func_stretch_int16_int64(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((int64_t *)(args.output_buffer))[i] = (((int64_t)(((int16_t *)(args.input_buffer))[i])) << 48);
    }
}

void converter_func_stretch_int32_int64(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((int64_t *)(args.output_buffer))[i] = (((int64_t)(((int32_t *)(args.input_buffer))[i])) << 32);
    }
}

void converter_func_shrink_double_float(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((float *)(args.output_buffer))[i] = ((float)(((double *)(args.input_buffer))[i]));
    }
}

void converter_func_shrink_int32_int16(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((int16_t *)(args.output_buffer))[i] = ((int16_t)((((int32_t *)(args.input_buffer))[i]) >> 16));
    }
}

void converter_func_shrink_int64_int32(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((int32_t *)(args.output_buffer))[i] = ((int32_t)((((int64_t *)(args.input_buffer))[i]) >> 32));
    }
}

void converter_func_shrink_int64_int16(void *state_ptr, audio_converter_args args) {
    (void) state_ptr;
    for(unsigned int i = 0; i < args.input_buffer_num_samples; i++) {
        ((int16_t *)(args.output_buffer))[i] = ((int16_t)((((int64_t *)(args.input_buffer))[i]) >> 48));
    }
}

audio_converter converter_constructor_shrink_stretch(audio_format *source_format, audio_format target_format) {

    audio_converter c = {
        .func = NULL,
        .state_ptr = NULL
    };

    c.func =
        (source_format->is_float && source_format->num_bits_per_channel == 32 &&target_format.num_bits_per_channel == 64) ? converter_func_stretch_float_double :
        (!source_format->is_float && source_format->num_bits_per_channel == 16 && target_format.num_bits_per_channel == 32) ? converter_func_stretch_int16_int32 :
        (!source_format->is_float && source_format->num_bits_per_channel == 16 && target_format.num_bits_per_channel == 64) ? converter_func_stretch_int16_int64 :
        (!source_format->is_float && source_format->num_bits_per_channel == 32 && target_format.num_bits_per_channel == 64) ? converter_func_stretch_int32_int64 :
        (source_format->is_float && source_format->num_bits_per_channel == 64 &&target_format.num_bits_per_channel == 32) ? converter_func_shrink_double_float :
        (!source_format->is_float && source_format->num_bits_per_channel == 32 && target_format.num_bits_per_channel == 16) ? converter_func_shrink_int32_int16 :
        (!source_format->is_float && source_format->num_bits_per_channel == 64 && target_format.num_bits_per_channel == 32) ? converter_func_shrink_int64_int32 :
        (!source_format->is_float && source_format->num_bits_per_channel == 64 && target_format.num_bits_per_channel == 16) ? converter_func_shrink_int64_int16 : NULL;

    source_format->num_bits_per_channel = target_format.num_bits_per_channel;

    return c;

}

void converter_func_merge_split_float(void *state_ptr, audio_converter_args args) {
    int source_num_channels = ((int *) state_ptr)[0];
    int target_num_channels = ((int *) state_ptr)[1];

    for(unsigned int frame_index = 0; frame_index < args.input_buffer_num_frames; frame_index++) {
        float value = 0;

        for(int channel_index = 0; channel_index < source_num_channels; channel_index++) {
            value += ((float *)(args.input_buffer))[(frame_index * source_num_channels) + channel_index];
        }

        value /= source_num_channels;

        for(int channel_index = 0; channel_index < target_num_channels; channel_index++) {
            ((float *)(args.output_buffer))[(frame_index * target_num_channels) + channel_index] = value;
        }
    }
}

void converter_func_merge_split_double(void *state_ptr, audio_converter_args args) {
    int source_num_channels = ((int *) state_ptr)[0];
    int target_num_channels = ((int *) state_ptr)[1];

    for(unsigned int frame_index = 0; frame_index < args.input_buffer_num_frames; frame_index++) {
        double value = 0;

        for(int channel_index = 0; channel_index < source_num_channels; channel_index++) {
            value += ((double *)(args.input_buffer))[(frame_index * source_num_channels) + channel_index];
        }

        value /= source_num_channels;

        for(int channel_index = 0; channel_index < target_num_channels; channel_index++) {
            ((double *)(args.output_buffer))[(frame_index * target_num_channels) + channel_index] = value;
        }
    }
}

void converter_func_merge_split_int16(void *state_ptr, audio_converter_args args) {
    int source_num_channels = ((int *) state_ptr)[0];
    int target_num_channels = ((int *) state_ptr)[1];

    for(unsigned int frame_index = 0; frame_index < args.input_buffer_num_frames; frame_index++) {
        int16_t value = 0;

        for(int channel_index = 0; channel_index < source_num_channels; channel_index++) {
            value += ((int16_t *)(args.input_buffer))[(frame_index * source_num_channels) + channel_index];
        }

        value /= source_num_channels;

        for(int channel_index = 0; channel_index < target_num_channels; channel_index++) {
            ((int16_t *)(args.output_buffer))[(frame_index * target_num_channels) + channel_index] = value;
        }
    }
}

void converter_func_merge_split_int32(void *state_ptr, audio_converter_args args) {
    int source_num_channels = ((int *) state_ptr)[0];
    int target_num_channels = ((int *) state_ptr)[1];

    for(unsigned int frame_index = 0; frame_index < args.input_buffer_num_frames; frame_index++) {
        int32_t value = 0;

        for(int channel_index = 0; channel_index < source_num_channels; channel_index++) {
            value += ((int32_t *)(args.input_buffer))[(frame_index * source_num_channels) + channel_index];
        }

        value /= source_num_channels;

        for(int channel_index = 0; channel_index < target_num_channels; channel_index++) {
            ((int32_t *)(args.output_buffer))[(frame_index * target_num_channels) + channel_index] = value;
        }
    }
}

void converter_func_merge_split_int64(void *state_ptr, audio_converter_args args) {
    int source_num_channels = ((int *) state_ptr)[0];
    int target_num_channels = ((int *) state_ptr)[1];

    for(unsigned int frame_index = 0; frame_index < args.input_buffer_num_frames; frame_index++) {
        int64_t value = 0;

        for(int channel_index = 0; channel_index < source_num_channels; channel_index++) {
            value += ((int64_t *)(args.input_buffer))[(frame_index * source_num_channels) + channel_index];
        }

        value /= source_num_channels;

        for(int channel_index = 0; channel_index < target_num_channels; channel_index++) {
            ((int64_t *)(args.output_buffer))[(frame_index * target_num_channels) + channel_index] = value;
        }
    }
}

audio_converter converter_constructor_merge_split(audio_format *source_format, audio_format target_format) {

    audio_converter c = {
        .func = NULL,
        .state_ptr = NULL
    };

    if(source_format->num_channels != target_format.num_channels && source_format->is_float == target_format.is_float && source_format->num_bits_per_channel == target_format.num_bits_per_channel) {
        c.func =
            (source_format->is_float && source_format->num_bits_per_channel == 32) ? converter_func_merge_split_float
            : (source_format->is_float && source_format->num_bits_per_channel == 64) ? converter_func_merge_split_double
            : (!source_format->is_float && source_format->num_bits_per_channel == 16) ? converter_func_merge_split_int16
            : (!source_format->is_float && source_format->num_bits_per_channel == 32) ? converter_func_merge_split_int32
            : (!source_format->is_float && source_format->num_bits_per_channel == 64) ? converter_func_merge_split_int64 : NULL;
    }

    if(c.func != NULL) {
        int num_channels[2] = {source_format->num_channels, target_format.num_channels};

        c.state_ptr = malloc(sizeof(num_channels));
        if(c.state_ptr == NULL) {
            c.func = NULL;
            return c;
        }

        memcpy(c.state_ptr, num_channels, sizeof(num_channels));
    }

    source_format->num_channels = target_format.num_channels;

    return c;

}

int get_resample_source_frame_index(unsigned int target_frame_index, int source_frame_rate, int target_frame_rate) {

    const long scale = 1024L;

    return (unsigned int)(((scale * target_frame_index * source_frame_rate) / target_frame_rate) / scale);

}

void converter_func_resample_float(void *state_ptr, audio_converter_args args) {

    resample_converter_state state = *((resample_converter_state *) state_ptr);

    for(unsigned int target_frame_index = 0; target_frame_index < args.output_buffer_num_frames; target_frame_index++) {
        unsigned int source_frame_index = get_resample_source_frame_index(target_frame_index, state.source_frame_rate, state.target_frame_rate);

        for(int channel_index = 0; channel_index < state.num_channels; channel_index++) {
            ((float *)(args.output_buffer))[(target_frame_index * state.num_channels) + channel_index] = ((float *)(args.input_buffer))[(source_frame_index * state.num_channels) + channel_index];
        }
    }

}

void converter_func_resample_double(void *state_ptr, audio_converter_args args) {

    resample_converter_state state = *((resample_converter_state *) state_ptr);

    for(unsigned int target_frame_index = 0; target_frame_index < args.output_buffer_num_frames; target_frame_index++) {
        unsigned int source_frame_index = get_resample_source_frame_index(target_frame_index, state.source_frame_rate, state.target_frame_rate);

        for(int channel_index = 0; channel_index < state.num_channels; channel_index++) {
            ((double *)(args.output_buffer))[(target_frame_index * state.num_channels) + channel_index] = ((double *)(args.input_buffer))[(source_frame_index * state.num_channels) + channel_index];
        }
    }

}

void converter_func_resample_int16(void *state_ptr, audio_converter_args args) {

    resample_converter_state state = *((resample_converter_state *) state_ptr);

    for(unsigned int target_frame_index = 0; target_frame_index < args.output_buffer_num_frames; target_frame_index++) {
        unsigned int source_frame_index = get_resample_source_frame_index(target_frame_index, state.source_frame_rate, state.target_frame_rate);

        for(int channel_index = 0; channel_index < state.num_channels; channel_index++) {
            ((int16_t *)(args.output_buffer))[(target_frame_index * state.num_channels) + channel_index] = ((int16_t *)(args.input_buffer))[(source_frame_index * state.num_channels) + channel_index];
        }
    }

}

void converter_func_resample_int32(void *state_ptr, audio_converter_args args) {

    resample_converter_state state = *((resample_converter_state *) state_ptr);

    for(unsigned int target_frame_index = 0; target_frame_index < args.output_buffer_num_frames; target_frame_index++) {
        unsigned int source_frame_index = get_resample_source_frame_index(target_frame_index, state.source_frame_rate, state.target_frame_rate);

        for(int channel_index = 0; channel_index < state.num_channels; channel_index++) {
            ((int32_t *)(args.output_buffer))[(target_frame_index * state.num_channels) + channel_index] = ((int32_t *)(args.input_buffer))[(source_frame_index * state.num_channels) + channel_index];
        }
    }

}

void converter_func_resample_int64(void *state_ptr, audio_converter_args args) {

    resample_converter_state state = *((resample_converter_state *) state_ptr);

    for(unsigned int target_frame_index = 0; target_frame_index < args.output_buffer_num_frames; target_frame_index++) {
        unsigned int source_frame_index = get_resample_source_frame_index(target_frame_index, state.source_frame_rate, state.target_frame_rate);

        for(int channel_index = 0; channel_index < state.num_channels; channel_index++) {
            ((int64_t *)(args.output_buffer))[(target_frame_index * state.num_channels) + channel_index] = ((int64_t *)(args.input_buffer))[(source_frame_index * state.num_channels) + channel_index];
        }
    }

}

audio_converter converter_constructor_resample(audio_format *source_format, audio_format target_format) {

    audio_converter c = {
        .func = NULL,
        .state_ptr = NULL
    };

    if(source_format->num_channels == target_format.num_channels && source_format->num_bits_per_channel == target_format.num_bits_per_channel && source_format->is_float == target_format.is_float && source_format->sample_rate != target_format.sample_rate) {
        c.func =
            (source_format->is_float && source_format->num_bits_per_channel == 32) ? converter_func_resample_float
            : (source_format->is_float && source_format->num_bits_per_channel == 64) ? converter_func_resample_double
            : (!source_format->is_float && source_format->num_bits_per_channel == 16) ? converter_func_resample_int16
            : (!source_format->is_float && source_format->num_bits_per_channel == 32) ? converter_func_resample_int32
            : (!source_format->is_float && source_format->num_bits_per_channel == 64) ? converter_func_resample_int64 : NULL;
    }

    if(c.func != NULL) {
        resample_converter_state state = {
            .num_channels = source_format->num_channels,
            .source_frame_rate = source_format->sample_rate,
            .target_frame_rate = target_format.sample_rate
        };

        c.state_ptr = malloc(sizeof(state));
        if(c.state_ptr == NULL) {
            c.func = NULL;
            return c;
        }

        memcpy(c.state_ptr, &state, sizeof(state));
    }

    source_format->sample_rate = target_format.sample_rate;

    return c;

}

bool add_converters(audio_conversion_info *info) {

    audio_format current_source_format = *(info->source_format);
    audio_format target_format = *(info->target_format);

    if(current_source_format.is_float && !target_format.is_float && (current_source_format.num_bits_per_channel == 32 || current_source_format.num_bits_per_channel == 64)) {
        if(!add_converter(converter_constructor_cast, &current_source_format, target_format, (audio_converter *) &(info->converters), &(info->num_converters), sizeof(info->converters)/sizeof(info->converters[0]))) {
            fprintf(stderr, "audio: add_converters: failed adding cast converter\n");
            return false;
        }
    }

    if(current_source_format.num_bits_per_channel != target_format.num_bits_per_channel) {
        if(!add_converter(converter_constructor_shrink_stretch, &current_source_format, target_format, (audio_converter *) &(info->converters), &(info->num_converters), sizeof(info->converters)/sizeof(info->converters[0]))) {
            fprintf(stderr, "audio: add_converters: failed adding shrink_stretch converter\n");
            return false;
        }
    }

    if((!!current_source_format.is_float) != (!!target_format.is_float)) {
        if(!add_converter(converter_constructor_cast, &current_source_format, target_format, (audio_converter *) &(info->converters), &(info->num_converters), sizeof(info->converters)/sizeof(info->converters[0]))) {
            fprintf(stderr, "audio: add_converters: failed adding cast converter\n");
            return false;
        }
    }

    if(current_source_format.num_channels != target_format.num_channels) {
        if(!add_converter(converter_constructor_merge_split, &current_source_format, target_format, (audio_converter *) &(info->converters), &(info->num_converters), sizeof(info->converters)/sizeof(info->converters[0]))) {
            fprintf(stderr, "audio: add_converters: failed adding merge_split converter\n");
            return false;
        }
    }

    if(current_source_format.sample_rate != target_format.sample_rate) {
        if(!add_converter(converter_constructor_resample, &current_source_format, target_format, (audio_converter *) &(info->converters), &(info->num_converters), sizeof(info->converters)/sizeof(info->converters[0]))) {
            fprintf(stderr, "audio: add_converters: failed adding resample converter\n");
            return false;
        }
    }

    if(!audio_format_equal(current_source_format, target_format)) {
        fprintf(stderr, "audio: add_converters: done adding converters, but formats still unequal: is_float: %d/%d, num_bits_per_channel: %d/%d, num_channels: %d/%d, sample_rate: %d/%d\n", current_source_format.is_float ? 1 : 0, target_format.is_float ? 1 : 0, current_source_format.num_bits_per_channel, target_format.num_bits_per_channel, current_source_format.num_channels, target_format.num_channels, current_source_format.sample_rate, target_format.sample_rate);
        return false;
    }

    return true;

}

gpointer conversion_thread_main(gpointer data) {

    audio_conversion_info *info = (audio_conversion_info *) data;

    const audio_format source_format = *(info->source_format);
    const audio_format target_format = *(info->target_format);

    if(!audio_format_validate_for_conversion(source_format)) {
        return NULL;
    }

    if(!audio_format_validate_for_conversion(target_format)) {
        return NULL;
    }

    const size_t source_format_frame_size_num_bytes = audio_format_get_frame_size_num_bytes(source_format);
    const size_t target_format_frame_size_num_bytes = audio_format_get_frame_size_num_bytes(target_format);
    const size_t max_frame_size_num_bytes = source_format_frame_size_num_bytes > target_format_frame_size_num_bytes ? source_format_frame_size_num_bytes : target_format_frame_size_num_bytes;

    const int max_frame_rate = source_format.sample_rate > target_format.sample_rate ? source_format.sample_rate : target_format.sample_rate;

    const int target_buffer_size_milliseconds = 32;

    const int max_read_num_frames = (((long) target_buffer_size_milliseconds) * source_format.sample_rate) / 1000;
    const size_t max_read_num_bytes = source_format_frame_size_num_bytes * max_read_num_frames;

    const size_t buffer_size_num_bytes = (((long) target_buffer_size_milliseconds) * max_frame_rate * max_frame_size_num_bytes) / 1000;

    uint8_t buffer_a[buffer_size_num_bytes];
    uint8_t buffer_b[buffer_size_num_bytes];

    for(;;) {

        gpointer data = g_async_queue_pop(info->source_async_queue);
        (void) data;

        size_t available_num_bytes;
        while((available_num_bytes = ring_buffer_output_get_num_bytes_available(info->source_ring_buffer)) > source_format_frame_size_num_bytes) {
            if(available_num_bytes > max_read_num_bytes) {
                available_num_bytes = max_read_num_bytes;
            }

            available_num_bytes -= (available_num_bytes % source_format_frame_size_num_bytes);

            size_t input_buffer_num_bytes = available_num_bytes;
            size_t input_buffer_num_frames = (input_buffer_num_bytes / source_format_frame_size_num_bytes);
            size_t input_buffer_num_samples = input_buffer_num_frames * source_format.num_channels;

            size_t output_buffer_num_frames = ((((1L << 20) * input_buffer_num_frames) / source_format.sample_rate) * target_format.sample_rate) / (1L << 20);
            size_t output_buffer_num_samples = output_buffer_num_frames * target_format.num_channels;
            size_t output_buffer_num_bytes = output_buffer_num_frames * target_format_frame_size_num_bytes;

            audio_converter_args args = {
                .input_buffer = buffer_a,
                .output_buffer = buffer_b,
                .input_buffer_num_bytes = input_buffer_num_bytes,
                .input_buffer_num_frames = input_buffer_num_frames,
                .input_buffer_num_samples = input_buffer_num_samples,
                .output_buffer_num_frames = output_buffer_num_frames,
                .output_buffer_num_samples = output_buffer_num_samples,
                .output_buffer_num_bytes = output_buffer_num_bytes
            };

            ring_buffer_output(info->source_ring_buffer, args.input_buffer, input_buffer_num_bytes);

            //printf("audio: conversion_thread_main: available_num_bytes=%lu\n", available_num_bytes);

            for(int i = 0; i < info->num_converters; i++) {
                audio_converter converter = info->converters[i];
                converter.func(converter.state_ptr, args);
                swap(&(args.input_buffer), &(args.output_buffer));
            }

            ring_buffer_input(info->target_ring_buffer, args.input_buffer, output_buffer_num_bytes);

        }

        GAsyncQueue *target_async_queue = info->target_async_queue;
        if(target_async_queue != NULL) {
            g_async_queue_push(target_async_queue, info);
        }

    }

    return NULL;

}

audio_record_stream *audio_record_stream_new() {

    ring_buffer *native_data_ring_buffer = NULL;
    GAsyncQueue *native_data_async_queue = NULL;
    GAsyncQueue *standard_data_async_queue = NULL;
    ring_buffer *standard_data_ring_buffer = NULL;
    audio_record_stream *stream = NULL;
    GThread *conversion_thread = NULL;

    native_data_async_queue = g_async_queue_new();
    if(native_data_async_queue == NULL) {
        print_error_message("Could not g_async_queue_new");
        goto abort;
    }

    native_data_ring_buffer = ring_buffer_new();
    if(native_data_ring_buffer == NULL) {
        print_error_message("Could not ring_buffer_new");
        goto abort;
    }

    stream = malloc(sizeof(audio_record_stream));
    if(stream == NULL) {
        print_error_message("Could not allocate audio_record_stream");
        goto abort;
    }

#ifdef SPRICH_OS_WIN
    SPRICH_AUDIO_SPECIFIC_RECORD_STREAM *specific_record_stream = audio_win_record_stream_new(native_data_ring_buffer, native_data_async_queue);
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
    SPRICH_AUDIO_SPECIFIC_RECORD_STREAM *specific_record_stream = audio_linux_record_stream_new(native_data_ring_buffer, native_data_async_queue);
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
    SPRICH_AUDIO_SPECIFIC_RECORD_STREAM *specific_record_stream = audio_macos_record_stream_new(native_data_ring_buffer, native_data_async_queue);
#endif // #ifdef SPRICH_OS_MACOS

    if(specific_record_stream == NULL) {
        goto abort;
    }

    printf("audio: audio_record_stream_new: format: is_float=%d num_channels=%d num_bits_per_channel=%d sample_rate=%d\n", specific_record_stream->format.is_float, specific_record_stream->format.num_channels, specific_record_stream->format.num_bits_per_channel, specific_record_stream->format.sample_rate);

    if(audio_format_needs_conversion(specific_record_stream->format)) {

        standard_data_async_queue = g_async_queue_new();
        if(standard_data_async_queue == NULL) {
            print_error_message("Could not g_async_queue_new");
            goto abort;
        }

        standard_data_ring_buffer = ring_buffer_new();
        if(standard_data_ring_buffer == NULL) {
            print_error_message("Could not ring_buffer_new");
            goto abort;
        }

    } else {

        standard_data_ring_buffer = native_data_ring_buffer;
        native_data_ring_buffer = NULL;
        standard_data_async_queue = native_data_async_queue;
        native_data_async_queue = NULL;

    }

    stream->specific_record_stream = specific_record_stream;
    stream->ring_buffer = standard_data_ring_buffer;
    stream->async_queue = standard_data_async_queue;
    stream->conversion_info.source_format = &(specific_record_stream->format);
    stream->conversion_info.source_ring_buffer = native_data_ring_buffer;
    stream->conversion_info.source_async_queue = native_data_async_queue;
    stream->conversion_info.target_format = audio_format_get_default();
    stream->conversion_info.target_ring_buffer = standard_data_ring_buffer;
    stream->conversion_info.target_async_queue = standard_data_async_queue;
    stream->conversion_info.thread = NULL;
    stream->conversion_info.num_converters = 0;

    if(native_data_ring_buffer != NULL && native_data_async_queue != NULL) {

        if(!add_converters(&(stream->conversion_info))) {
            goto abort;
        }

        conversion_thread = g_thread_try_new("conversion_thread_input", conversion_thread_main, &(stream->conversion_info), NULL);

        if(conversion_thread == NULL) {
            print_error_message("Could not g_thread_try_new");
            goto abort;
        }

    }

    stream->conversion_info.thread = conversion_thread;

    return stream;

    abort:
    free(stream);
    if(standard_data_ring_buffer != NULL) {
        ring_buffer_free(standard_data_ring_buffer);
    }
    if(native_data_ring_buffer != NULL) {
        ring_buffer_free(native_data_ring_buffer);
    }
    if(standard_data_async_queue != NULL) {
        g_async_queue_unref(standard_data_async_queue);
    }
    if(native_data_async_queue != NULL) {
        g_async_queue_unref(native_data_async_queue);
    }

    return NULL;

}

audio_playback_stream *audio_playback_stream_new() {

    ring_buffer *native_data_ring_buffer = NULL;
    GAsyncQueue *standard_data_async_queue = NULL;
    ring_buffer *standard_data_ring_buffer = NULL;
    audio_playback_stream *stream = NULL;
    GThread *conversion_thread = NULL;

    native_data_ring_buffer = ring_buffer_new();
    if(native_data_ring_buffer == NULL) {
        print_error_message("Could not ring_buffer_new");
        goto abort;
    }

    stream = malloc(sizeof(audio_playback_stream));
    if(stream == NULL) {
        print_error_message("Could not allocate audio_playback_stream");
        goto abort;
    }

#ifdef SPRICH_OS_WIN
    SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM *specific_playback_stream = audio_win_playback_stream_new(native_data_ring_buffer);
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
    SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM *specific_playback_stream = audio_linux_playback_stream_new(native_data_ring_buffer);
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
    SPRICH_AUDIO_SPECIFIC_PLAYBACK_STREAM *specific_playback_stream = audio_macos_playback_stream_new(native_data_ring_buffer);
#endif // #ifdef SPRICH_OS_MACOS

    if(specific_playback_stream == NULL) {
        goto abort;
    }

    printf("audio: audio_playback_stream_new: format: is_float=%d num_channels=%d num_bits_per_channel=%d sample_rate=%d\n", specific_playback_stream->format.is_float, specific_playback_stream->format.num_channels, specific_playback_stream->format.num_bits_per_channel, specific_playback_stream->format.sample_rate);

    if(audio_format_needs_conversion(specific_playback_stream->format)) {

        standard_data_async_queue = g_async_queue_new();
        if(standard_data_async_queue == NULL) {
            print_error_message("Could not g_async_queue_new");
            goto abort;
        }

        standard_data_ring_buffer = ring_buffer_new();
        if(standard_data_ring_buffer == NULL) {
            print_error_message("Could not ring_buffer_new");
            goto abort;
        }

    } else {

        standard_data_ring_buffer = native_data_ring_buffer;
        native_data_ring_buffer = NULL;

    }

    stream->specific_playback_stream = specific_playback_stream;
    stream->ring_buffer = standard_data_ring_buffer;
    stream->async_queue = standard_data_async_queue;
    stream->conversion_info.source_format = audio_format_get_default();
    stream->conversion_info.source_ring_buffer = standard_data_ring_buffer;
    stream->conversion_info.source_async_queue = standard_data_async_queue;
    stream->conversion_info.target_format = &(specific_playback_stream->format);
    stream->conversion_info.target_ring_buffer = native_data_ring_buffer;
    stream->conversion_info.target_async_queue = NULL;
    stream->conversion_info.thread = NULL;
    stream->conversion_info.num_converters = 0;

    if(native_data_ring_buffer != NULL && standard_data_async_queue != NULL) {

        if(!add_converters(&(stream->conversion_info))) {
            goto abort;
        }

        conversion_thread = g_thread_try_new("conversion_thread_output", conversion_thread_main, &(stream->conversion_info), NULL);

        if(conversion_thread == NULL) {
            print_error_message("Could not g_thread_try_new");
            goto abort;
        }

    }

    return stream;

    abort:
    free(stream);
    if(native_data_ring_buffer != NULL) {
        ring_buffer_free(native_data_ring_buffer);
    }
    if(standard_data_ring_buffer != NULL) {
        ring_buffer_free(standard_data_ring_buffer);
    }
    if(standard_data_async_queue != NULL) {
        g_async_queue_unref(standard_data_async_queue);
    }

    return NULL;

}

void audio_record_stream_free(audio_record_stream *audio_record_stream) {

    if(audio_record_stream != NULL) {

#ifdef SPRICH_OS_WIN
        audio_win_stream_free(audio_record_stream->specific_record_stream);
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
        audio_linux_stream_free(audio_record_stream->specific_record_stream);
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
        audio_macos_record_stream_free(audio_record_stream->specific_record_stream);
#endif // #ifdef SPRICH_OS_MACOS

        ring_buffer_free(audio_record_stream->ring_buffer);
        audio_record_stream->ring_buffer = NULL;

        if(audio_record_stream->conversion_info.source_ring_buffer != NULL) {
            ring_buffer_free(audio_record_stream->conversion_info.source_ring_buffer);
            audio_record_stream->conversion_info.source_ring_buffer = NULL;
        }

        g_async_queue_unref(audio_record_stream->async_queue);
        audio_record_stream->async_queue = NULL;

        if(audio_record_stream->conversion_info.source_async_queue != NULL) {
            g_async_queue_unref(audio_record_stream->conversion_info.source_async_queue);
            audio_record_stream->conversion_info.source_async_queue = NULL;
        }

        free(audio_record_stream);

    }

}

void audio_playback_stream_free(audio_playback_stream *audio_playback_stream) {

    if(audio_playback_stream != NULL) {

#ifdef SPRICH_OS_WIN
        audio_win_stream_free(audio_playback_stream->specific_playback_stream);
#endif // #ifdef SPRICH_OS_WIN

#ifdef SPRICH_OS_LINUX
        audio_linux_stream_free(audio_playback_stream->specific_playback_stream);
#endif // #ifdef SPRICH_OS_LINUX

#ifdef SPRICH_OS_MACOS
        audio_macos_playback_stream_free(audio_playback_stream->specific_playback_stream);
#endif // #ifdef SPRICH_OS_MACOS

        ring_buffer_free(audio_playback_stream->ring_buffer);
        audio_playback_stream->ring_buffer = NULL;

        free(audio_playback_stream);

    }

}
