#ifndef SPRICH_AUDIO_MACOS_H
#define SPRICH_AUDIO_MACOS_H

#ifdef SPRICH_OS_MACOS

#include <glib.h>
#include <CoreServices/CoreServices.h>
#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>
#include <AudioUnit/AudioUnit.h>
#include <CoreAudio/CoreAudioTypes.h>
#include <CoreFoundation/CoreFoundation.h>

#include "ring_buffer.h"

typedef struct {
    ring_buffer *ring_buffer;
    GAsyncQueue *async_queue;
    AudioUnit audio_unit;
    AudioBufferList *audio_buffer_list;
    void *buffer;
    size_t buffer_size_bytes;
    int num_bytes_per_sample;
    audio_format format;
} audio_macos_stream;

#include "audio.h"

bool audio_macos_init();
audio_macos_stream *audio_macos_record_stream_new(ring_buffer *ring_buffer, GAsyncQueue *record_stream_async_queue);
audio_macos_stream *audio_macos_playback_stream_new(ring_buffer *ring_buffer);
void audio_macos_record_stream_free(audio_macos_stream *stream);
void audio_macos_playback_stream_free(audio_macos_stream *stream);
void audio_macos_stop();

#endif // #ifdef SPRICH_OS_MACOS

#endif // #ifndef SPRICH_AUDIO_MACOS_H
