
CC = gcc
CPPC = g++
DEFS =
IFLAGS =
CPPIFLAGS =
CFLAGS =
CPPFLAGS =
LDFLAGS =
LIBS = glib-2.0 gio-2.0

FOR_OS = linux

ifeq ($(OS), Windows_NT)
	FOR_OS = win
else
	UNAME_S = $(shell uname -s)
	ifeq ($(UNAME_S), Darwin)
		FOR_OS = macos
	endif
endif
ifneq ($(shell readlink "`which $(CC)`" | grep "mingw"),)
	# gcc is a symlink, assume we're inside the win docker container (ugly, I know)
	FOR_OS = win
	CPPIFLAGS += -I/usr/x86_64-w64-mingw32/include/
endif

ifeq ($(FOR_OS), win)
	DEFS += -D SPRICH_OS_WIN
	LDFLAGS += -lole32
else
	ifeq ($(FOR_OS), macos)
		DEFS += -D SPRICH_OS_MACOS
		LDFLAGS += -framework CoreAudio -framework AudioToolbox
	else
		DEFS += -D SPRICH_OS_LINUX
		LIBS += portaudio-2.0
	endif
endif

IFLAGS += $(shell pkg-config --cflags $(LIBS))
CFLAGS += -fpic -g -std=c99 -Wall -Wextra -pedantic $(DEFS) $(IFLAGS)
CPPFLAGS += -fpic -g -std=c++17 -Wall -Wextra -pedantic $(DEFS) $(IFLAGS) $(CPPIFLAGS)
LDFLAGS += $(shell pkg-config --libs $(LIBS))

.PHONY: all win-dll-copy win-release clean

all: build/client build/server

build/%.o: src/%.c src/%.h src/constants.h
	$(CC) $(CFLAGS) -c -o $@ $<

build/%.opp: src/%.cpp src/%.h src/constants.h
	$(CPPC) $(CPPFLAGS) -c -o $@ $<

build/audio.o: src/pcm_code.h src/audio_format.h src/audio_win.h src/audio_macos.h src/audio_linux.h

build/client.o: src/ring_buffer.h src/socket.h src/audio.h

build/server.o: src/ring_buffer.h src/socket.h src/pcm_code.h

build/client: build/client.o build/ring_buffer.o build/socket.o build/audio.o build/audio_format.o build/audio_win.opp build/audio_linux.o build/audio_macos.o build/pcm_code.o
	$(CC) -o $@ $^ $(LDFLAGS)

build/server: build/server.o build/ring_buffer.o build/socket.o build/pcm_code.o
	$(CC) -o $@ $^ $(LDFLAGS)

win-dll-copy:
	cp /build/glib/_build/glib/libglib-2.0-0.dll build/
	cp /build/glib/_build/gio/libgio-2.0-0.dll build/
	cp /build/glib/_build/gmodule/libgmodule-2.0-0.dll build/
	cp /build/glib/_build/gobject/libgobject-2.0-0.dll build/
	cp /build/glib/_build/subprojects/proxy-libintl/libintl.dll build/
	cp /build/glib/_build/subprojects/libffi/src/libffi-7.dll build/
	cp /build/glib/_build/subprojects/zlib-1.2.11/libz.dll build/

win-release: build/sprich.zip

build/sprich.zip: build/client build/server win-dll-copy
	cd build \
	&& mkdir -p sprich \
	&& cp *.dll *.exe sprich/ \
	&& zip -r sprich.zip sprich/ \
	&& rm -r sprich/

clean:
	rm -f build/*.o build/*.opp build/client build/client.exe build/server build/server.exe build/*.dll build/*.zip
	rm -rf build/sprich/
